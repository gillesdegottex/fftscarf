#ifndef __FFTSCARF_FFTS_H__
#define __FFTSCARF_FFTS_H__

#ifdef FFTSCARF_FFT_FFTS

#ifdef HAVE_SSE
#include <xmmintrin.h>
#endif
#include <stdlib.h>

#include <cmath>
#include <complex>
#include <vector>
#include <string>
#include <cmath>
#include <iostream>

//#include <ffts/include/ffts.h> // Will be included in fftscarf.h
//#include <ffts/src/ffts_attributes.h> // Will be included in fftscarf.h

namespace fftscarf {

class FFTPlanFFTS : public FFTPlanImplementation
{
public:
    typedef float FloatType;

private:
    FloatType FFTS_ALIGN(32) *m_signal;
    FloatType FFTS_ALIGN(32) *m_spec;
    ffts_plan_t *m_p;
    FFTSCARF_PLAN_ACCESS_DECLARE

public:
    static inline std::string version(){
        return std::string("0.9.0"); // This is the current built-in version
    }
    static inline std::string libraryName(){
        std::stringstream result;
        result << "FFTS " << version() << " (precision " << 8*sizeof(FloatType) << "b)"; // This is the current built-in version
        return result.str();
    }

    FFTPlanFFTS(bool forward=true)
        : FFTPlanImplementation(forward)
    {
        m_p = NULL;
        m_signal = NULL;
        m_spec = NULL;

        m_p = NULL;
    }
    FFTPlanFFTS(int n, bool forward=true)
        : FFTPlanImplementation(n, forward)
    {
        m_p = NULL;
        m_signal = NULL;
        m_spec = NULL;

        resize(n);
    }

    virtual ~FFTPlanFFTS(){
        FFTSCARF_PLAN_ACCESS_DTOR

        if(m_signal || m_spec){
            #if (defined(_WIN32) || defined(WIN32))
                _aligned_free(m_signal);
                _aligned_free(m_spec);
            #else
                #ifdef HAVE_SSE
                    _mm_free(m_signal);
                    _mm_free(m_spec);
                #else
                    free(m_signal);
                    free(m_spec);
                #endif
            #endif
            m_signal = NULL;
            m_spec = NULL;
        }

        if(m_p){
            ffts_free(m_p);
            m_p = NULL;
        }
    }

    virtual void resize(int n)
    {
        if(n==m_size) return;

        assert(n>0);
    //    assert(n<=65536);
        assert(isPow2(n));

        FFTSCARF_PLAN_ACCESS_GUARD
        m_size = n;

        if(m_signal || m_spec){
            #ifdef HAVE_SSE
                _mm_free(m_signal);
                _mm_free(m_spec);
            #else
                free(m_signal);
                free(m_spec);
            #endif
            m_signal = NULL;
            m_spec = NULL;
        }

        #if (defined(_WIN32) || defined(WIN32))
            m_signal = (FloatType FFTS_ALIGN(32) *) _aligned_malloc(2 * m_size * sizeof(FloatType), 32);
            m_spec = (FloatType FFTS_ALIGN(32) *) _aligned_malloc(2 * m_size * sizeof(FloatType), 32);
        #else
            // See http://www.delorie.com/gnu/docs/glibc/libc_31.html
            // Or ffts/tests/test.c
            #ifdef HAVE_SSE
                m_signal = (FloatType FFTS_ALIGN(32) *) _mm_malloc(2 * m_size * sizeof(FloatType), 32);
                m_spec = (FloatType FFTS_ALIGN(32) *) _mm_malloc(2 * m_size * sizeof(FloatType), 32);
            #else
                m_signal = (FloatType FFTS_ALIGN(32) *) valloc(2 * m_size * sizeof(FloatType));
                m_spec = (FloatType FFTS_ALIGN(32) *) valloc(2 * m_size * sizeof(FloatType));
            #endif
        #endif

        if(m_p){
            ffts_free(m_p);
            m_p = NULL;
        }

        if(m_forward)
            m_p = ffts_init_1d_real(n, FFTS_FORWARD); // TODO sign for the backward I suppose
        else
            m_p = ffts_init_1d_real(n, FFTS_BACKWARD); // TODO sign for the backward I suppose
    }

    template<typename TypeInContainer, typename TypeOutContainer>
    void rfft(const TypeInContainer& in, TypeOutContainer& out, int dftlen=-1){
        if (!m_forward)
            throw std::runtime_error("A backward IDFT FFTPlan cannot compute the forward DFT");

        if(dftlen>0 && m_size!=dftlen)
            resize(dftlen);

        dftlen = m_size;

        assert(in.size() <= dftlen);

        int neededoutsize = (m_size%2==1)?(m_size-1)/2+1:m_size/2+1;
        if(int(out.size())!=neededoutsize)
            out.resize(neededoutsize);

        int u = 0;
        for(; u<int(in.size()); ++u)
            m_signal[u] = in[u];
        for(; u<dftlen; ++u)
            m_signal[u] = 0.0;

        {
            FFTSCARF_PLAN_ACCESS_GUARD
            ffts_execute(m_p, m_signal, m_spec);
        }

        for(int f=0; f<m_size/2+1; f++)
            out[f] = make_complex(m_spec[2*f], m_spec[2*f+1]);
    }

    template<typename TypeInContainer, typename TypeOutContainer>
    void irfft(const TypeInContainer& in, TypeOutContainer& out, int winlen=-1){
        if(m_forward)
            throw std::runtime_error("A forward DFT FFTPlan cannot compute the backward IDFT");

        int dftlen = (in.size()-1)*2;
        if(m_size!=dftlen)
            resize(dftlen);

        if(winlen==-1)
            winlen = m_size;

        if(int(out.size())!=winlen)
            out.resize(winlen);

        for(int f=0; f<m_size/2+1; f++){
            m_spec[2*f] = in[f].real();
            m_spec[2*f+1] = in[f].imag();
        }

        {
            FFTSCARF_PLAN_ACCESS_GUARD
            ffts_execute(m_p, m_spec, m_signal);
        }

        FloatType oneoverdftlen = FloatType(1.0)/m_size;
        for(int u=0; u<winlen; ++u)
            out[u] = m_signal[u]*oneoverdftlen;
    }
};

typedef FFTPlanFFTS FFTPlanSingleFFTS;
#ifndef FFTSCARF_FFTPLANSINGLE
    #define FFTSCARF_FFTPLANSINGLE
    typedef FFTPlanSingleFFTS FFTPlanSingle;
#endif
#ifndef FFTSCARF_FFTPLAN
    #define FFTSCARF_FFTPLAN
    typedef FFTPlanFFTS FFTPlan;
#endif
}

#endif // FFTSCARF_FFT_FFTS

#endif // __FFTSCARF_FFTS_H__
