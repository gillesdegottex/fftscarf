#ifndef __FFTSCARF_FFT_PFFFT_H__
#define __FFTSCARF_FFT_PFFFT_H__

#ifdef FFTSCARF_FFT_PFFFT

#include <cmath>
#include <complex>
#include <vector>
#include <string>
#include <cmath>
#include <iostream>

//#include <pffft/pffft.h> // Will be included in fftscarf.h

namespace fftscarf {

class FFTPlanPFFFT : public FFTPlanImplementation
{
public:
    typedef float FloatType;

private:
    PFFFT_Setup *m_setup;
    FloatType *m_input;
    FloatType *m_output;
    FloatType *m_work;
    FFTSCARF_PLAN_ACCESS_DECLARE

public:
    static inline std::string version(){
        return std::string("2014-08-10"); // This is the current built-in version
    }
    static inline std::string libraryName(){
        std::stringstream result;
        result << "Pretty Fast FFT (PFFFT) " << version() << " (SIMD size " << pffft_simd_size() << ")" << " (precision " << 8*sizeof(FloatType) << "b)"; // This is the current built-in version
        return result.str();
    }

    FFTPlanPFFFT(bool forward=true)
        : FFTPlanImplementation(forward)
    {
        m_setup = NULL;
        m_input = NULL;
        m_output = NULL;
        m_work = NULL;
    }
    FFTPlanPFFFT(int n, bool forward=true)
        : FFTPlanImplementation(n, forward)
    {
        m_setup = NULL;
        m_input = NULL;
        m_output = NULL;
        m_work = NULL;

        resize(n);
    }
    virtual ~FFTPlanPFFFT(){
        FFTSCARF_PLAN_ACCESS_DTOR
        if(m_setup || m_input || m_output){
            pffft_destroy_setup(m_setup);
            m_setup = NULL;
            pffft_aligned_free(m_input);
            m_input = NULL;
            pffft_aligned_free(m_output);
            m_output = NULL;
        }
        if(m_work){
            pffft_aligned_free(m_work);
            m_work = NULL;
        }
    }

    virtual void resize(int n)
    {
        if(n==m_size) return;

        assert(n>0);
        assert(n>=32);
        assert(isPow235(n));

        FFTSCARF_PLAN_ACCESS_GUARD
        m_size = n;

        if(m_setup || m_input || m_output){
            pffft_destroy_setup(m_setup);
            m_setup = NULL;
            pffft_aligned_free(m_input);
            m_input = NULL;
            pffft_aligned_free(m_output);
            m_output = NULL;
        }
        if(m_work){
            pffft_aligned_free(m_work);
            m_work = NULL;
        }

        m_setup = pffft_new_setup(n, PFFFT_REAL);

        m_input = (FloatType*)pffft_aligned_malloc(n*sizeof(FloatType));
        m_output = (FloatType*)pffft_aligned_malloc(n*sizeof(FloatType));

        // Following the PFFFT's documentation
        if(n>=16384)
            m_work = (FloatType*)pffft_aligned_malloc(n*sizeof(FloatType));
    }

    template<typename TypeInContainer, typename TypeOutContainer>
    void rfft(const TypeInContainer& in, TypeOutContainer& out, int dftlen=-1){
        if (!m_forward)
            throw std::runtime_error("A backward IDFT FFTPlan cannot compute the forward DFT");

        if(dftlen>0 && m_size!=dftlen)
            resize(dftlen);

        dftlen = m_size;

        assert(in.size() <= dftlen);

        int neededoutsize = (m_size%2==1)?(m_size-1)/2+1:m_size/2+1;
        if(int(out.size())!=neededoutsize)
            out.resize(neededoutsize);

        int u = 0;
        for(; u<int(in.size()); ++u)
            m_input[u] = in[u];
        for(; u<dftlen; ++u)
            m_input[u] = 0.0;

        {
            FFTSCARF_PLAN_ACCESS_GUARD
            pffft_transform_ordered(m_setup, m_input, m_output, m_work, PFFFT_FORWARD);
        }

        out[0] = m_output[0]; // DC
        for(int f=1; f<m_size/2; f++)
            out[f] = make_complex(m_output[2*f], m_output[2*f+1]);
        out[m_size/2] = m_output[1]; // Nyquist
    }

    template<typename TypeInContainer, typename TypeOutContainer>
    void irfft(const TypeInContainer& in, TypeOutContainer& out, int winlen=-1){
        if(m_forward)
            throw std::runtime_error("A forward DFT FFTPlan cannot compute the backward IDFT");

        int dftlen = (in.size()-1)*2;
        if(m_size!=dftlen)
            resize(dftlen);

        if(winlen==-1)
            winlen = m_size;

        if(int(out.size())!=winlen)
            out.resize(winlen);

        m_input[0] = in[0].real(); // DC
        m_input[1] = in[m_size/2].real(); // Nyquist
        for(int f=1; f<m_size/2; f++){
            m_input[2*f] = in[f].real();
            m_input[2*f+1] = in[f].imag();
        }

        {
            FFTSCARF_PLAN_ACCESS_GUARD
            pffft_transform_ordered(m_setup, m_input, m_output, m_work, PFFFT_BACKWARD);
        }

        FloatType oneoverdftlen = FloatType(1.0)/m_size;
        for(int u=0; u<winlen; ++u)
            out[u] = m_output[u]*oneoverdftlen;
    }
};

typedef FFTPlanPFFFT FFTPlanSinglePFFFT;
#ifndef FFTSCARF_FFTPLANSINGLE
    #define FFTSCARF_FFTPLANSINGLE
    typedef FFTPlanSinglePFFFT FFTPlanSingle;
#endif
#ifndef FFTSCARF_FFTPLAN
    #define FFTSCARF_FFTPLAN
    typedef FFTPlanPFFFT FFTPlan;
#endif
}

#endif // FFTSCARF_FFT_PFFFT

#endif // __FFTSCARF_FFT_PFFFT_H__
