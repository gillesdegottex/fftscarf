#ifndef __FFTSCARF_OOURA_H__
#define __FFTSCARF_OOURA_H__

#ifdef FFTSCARF_FFT_OOURA

#include <cmath>
#include <complex>
#include <vector>
#include <string>

//extern "C" {
//#include <ooura/fftsg.h> // Will be included in fftscarf.h
//}

namespace fftscarf {

class FFTPlanOoura : public FFTPlanImplementation
{
public:
    typedef OOFLOAT FloatType;

private:
    FloatType* m_ooura_a = nullptr;
    FloatType* m_ooura_a_xcorr = nullptr;
    int* m_ooura_ip = nullptr;
    FloatType* m_ooura_w = nullptr;

    FFTSCARF_PLAN_ACCESS_DECLARE

    inline void clear(){
        if(m_ooura_a){
            delete[] m_ooura_a;
            m_ooura_a = nullptr;
        }
        if(m_ooura_a_xcorr){
            delete[] m_ooura_a_xcorr;
            m_ooura_a_xcorr = nullptr;
        }
        if(m_ooura_ip){
            delete[] m_ooura_ip;
            m_ooura_ip = nullptr;
        }
        if(m_ooura_w){
            delete[] m_ooura_w;
            m_ooura_w = nullptr;
        }
    }


public:
    static inline std::string version(){
        return std::string("2006.12"); // This is the current built-in version
    }
    static inline std::string libraryName(){
        std::stringstream result;
        result << "Ooura " << version() << " (precision " << 8*sizeof(FloatType) << "b)"; // This is the current built-in version
        return result.str();
    }

    FFTPlanOoura(bool forward=true)
        : FFTPlanImplementation(forward)
    {
    }
    FFTPlanOoura(int n, bool forward=true)
        : FFTPlanImplementation(n, forward)
    {
        resize(n);
    }

    virtual ~FFTPlanOoura(){
        FFTSCARF_PLAN_ACCESS_DTOR
        clear();
    }

    virtual void resize(int n)
    {
        if(n==m_size) return;

        assert(n>0);
        assert(isPow2(n));

        FFTSCARF_PLAN_ACCESS_GUARD
        m_size = n;

        clear();

        m_ooura_a = new FloatType[m_size];
        m_ooura_a_xcorr = new FloatType[m_size];
        m_ooura_ip = new int[2+(1<<(int)(std::log(m_size/2+0.5)/std::log(2.0))/2)];
        m_ooura_w = new FloatType[m_size/2];
        m_ooura_ip[0] = 0; // first time only
        rdft(m_size, 1, m_ooura_a, m_ooura_ip, m_ooura_w); // init cos/sin table
    }

    template<typename TypeInContainer, typename TypeOutContainer>
    void rfft(std::true_type, const TypeInContainer& in, TypeOutContainer& out, int dftlen=-1){
        if (!m_forward)
            throw std::runtime_error("A backward IDFT FFTPlan cannot compute the forward DFT");

        if(dftlen>0 && m_size!=dftlen)
            resize(dftlen);

        dftlen = m_size;

        assert(in.size() <= dftlen);

        int neededoutsize = (m_size%2==1)?(m_size-1)/2+1:m_size/2+1;
        if(int(out.size())!=neededoutsize)
            out.resize(neededoutsize);

        int u = 0;
        int len = int(in.size());
        for(; u<len; ++u)
            m_ooura_a[u] = in[u];
        for(; u<dftlen; ++u)
            m_ooura_a[u] = FloatType(0.0);

        {
            FFTSCARF_PLAN_ACCESS_GUARD
            rdft(m_size, 1, m_ooura_a, m_ooura_ip, m_ooura_w);
        }

        out[0] = m_ooura_a[0]; // DC
        for(int f=1; f<m_size/2; f++){
            out[f].real(m_ooura_a[2*f]);
            out[f].imag(-m_ooura_a[2*f+1]);
        }
        out[m_size/2] = m_ooura_a[1]; // Nyquist
    }
    // This is the case when the output container is real.
    template<typename TypeInContainer, typename TypeOutContainer>
    void rfft(std::false_type, const TypeInContainer& in, TypeOutContainer& out, int dftlen=-1){
        if (!m_forward)
            throw std::runtime_error("A backward IDFT FFTPlan cannot compute the forward DFT");

        if(dftlen>0 && m_size!=dftlen)
            resize(dftlen);

        dftlen = m_size;

        assert(in.size() <= dftlen);

        int neededoutsize = (m_size%2==1)?(m_size-1)/2+1:m_size/2+1;
        if(int(out.size())!=neededoutsize)
            out.resize(neededoutsize);

        int u = 0;
        int len = int(in.size());
        for(; u<len; ++u)
            m_ooura_a[u] = in[u];
        for(; u<dftlen; ++u)
            m_ooura_a[u] = FloatType(0.0);

        {
            FFTSCARF_PLAN_ACCESS_GUARD
            rdft(m_size, 1, m_ooura_a, m_ooura_ip, m_ooura_w);
        }

        out[0] = m_ooura_a[0]; // DC
        for(int f=1; f<m_size/2; f++){
            out[f] = std::hypot(m_ooura_a[2*f],m_ooura_a[2*f+1]); // Can ignore the '-'
        }
        out[m_size/2] = m_ooura_a[1]; // Nyquist
    }
    template<typename TypeInContainer, typename TypeOutContainer>
    void rfft(const TypeInContainer& in, TypeOutContainer& out, int dftlen=-1){
        rfft(is_container_complex<TypeOutContainer>{}, in, out, dftlen);
    }

    template<typename TypeInContainer, typename TypeOutContainer>
    void irfft(std::true_type, const TypeInContainer& in, TypeOutContainer& out, int winlen=-1){
        if(m_forward)
            throw std::runtime_error("A forward DFT FFTPlan cannot compute the backward IDFT");

        int dftlen = int((in.size()-1)*2);
        if(m_size!=dftlen)
            resize(dftlen);

        if(winlen==-1)
            winlen = m_size;

        if(int(out.size())!=winlen)
            out.resize(winlen);

        m_ooura_a[0] = in[0].real(); // DC
        for(int f=1; f<m_size/2; f++){
            m_ooura_a[2*f] = in[f].real();
            m_ooura_a[2*f+1] = -in[f].imag();
        }
        m_ooura_a[1] = in[m_size/2].real(); // Nyquist

        {
            FFTSCARF_PLAN_ACCESS_GUARD
            rdft(m_size, -1, m_ooura_a, m_ooura_ip, m_ooura_w);
        }

        FloatType oneoverdftlen = FloatType(2.0)/m_size;
        for(int u=0; u<winlen; ++u)
            out[u] = m_ooura_a[u]*oneoverdftlen;
    }
    //! This one assumes the input is real
    // TODO SPEEDUP with 144_FFT symmetry.pdf LAWRENCE R. RABINER, IEEE TRANSACTIONS ON ACOUSTICS, SPEECH, AND SIGNAL PROCESSING, VOL. ASSP-27, NO. 3, JUNE 1979
    template<typename TypeInContainer, typename TypeOutContainer>
    void irfft(std::false_type, const TypeInContainer& in, TypeOutContainer& out, int winlen=-1){
        if(m_forward)
            throw std::runtime_error("A forward DFT FFTPlan cannot compute the backward IDFT");

        int dftlen = int((in.size()-1)*2);
        if(m_size!=dftlen)
            resize(dftlen);

        if(winlen==-1)
            winlen = m_size;

        if(int(out.size())!=winlen)
            out.resize(winlen);

        m_ooura_a[0] = in[0]; // DC
        for(int f=1; f<m_size/2; f++){
            m_ooura_a[2*f] = in[f];
            m_ooura_a[2*f+1] = FloatType(0.0);
        }
        m_ooura_a[1] = in[m_size/2]; // Nyquist

        {
            FFTSCARF_PLAN_ACCESS_GUARD
            rdft(m_size, -1, m_ooura_a, m_ooura_ip, m_ooura_w);
        }

        FloatType oneoverdftlen = FloatType(2.0)/m_size;
        for(int u=0; u<winlen; ++u)
            out[u] = m_ooura_a[u]*oneoverdftlen;
    }
    template<typename TypeInContainer, typename TypeOutContainer>
    void irfft(const TypeInContainer& in, TypeOutContainer& out, int winlen=-1){
        irfft(is_container_complex<TypeInContainer>{}, in, out, winlen);
    }


    template<typename TypeInContainer, typename TypeOutContainer>
    void rcrosscorr(const TypeInContainer& in1, const TypeInContainer& in2, TypeOutContainer& out, int dftlen=-1) {

        if(dftlen>0 && m_size!=dftlen)
            resize(dftlen);

        dftlen = m_size;

        int u = 0;
        int len1 = int(in1.size());
        for(; u<len1; ++u)
            m_ooura_a[u] = in1[u];
        for(; u<dftlen; ++u)
            m_ooura_a[u] = FloatType(0.0);

        u = 0;
        int len2 = int(in2.size());
        for(; u<len2; ++u)
            m_ooura_a_xcorr[u] = in2[u];
        for(; u<dftlen; ++u)
            m_ooura_a_xcorr[u] = FloatType(0.0);

        {
            FFTSCARF_PLAN_ACCESS_GUARD
            rdft(m_size, 1, m_ooura_a, m_ooura_ip, m_ooura_w);
            rdft(m_size, 1, m_ooura_a_xcorr, m_ooura_ip, m_ooura_w);
        }

        m_ooura_a[0] = m_ooura_a[0]*m_ooura_a_xcorr[0]; // DC
        for(int f=1; f<m_size/2; f++){
            m_ooura_a_xcorr[2*f+1] *= FloatType(-1.0);
            FloatType real = m_ooura_a[2*f]*m_ooura_a_xcorr[2*f] - m_ooura_a[2*f+1]*m_ooura_a_xcorr[2*f+1];
            m_ooura_a[2*f+1] = m_ooura_a[2*f]*m_ooura_a_xcorr[2*f+1] + m_ooura_a[2*f+1]*m_ooura_a_xcorr[2*f];
            m_ooura_a[2*f] = real;
        }
        m_ooura_a[1] = m_ooura_a[1]*m_ooura_a_xcorr[1]; // Nyquist

        {
            FFTSCARF_PLAN_ACCESS_GUARD
            rdft(m_size, -1, m_ooura_a, m_ooura_ip, m_ooura_w);
        }

        FloatType oneoverdftlen = FloatType(2.0)/m_size;
        if(int(out.size())!=dftlen)
            out.resize(dftlen);
        for(int u=0; u<dftlen; ++u) {
            out[u] = m_ooura_a[u]*oneoverdftlen;
        }
    }
};

#if (FFTSCARF_PRECISION_DEFAULT == 32)
    typedef FFTPlanOoura FFTPlanSingleOoura;
    #ifndef FFTSCARF_FFTPLANSINGLE
        #define FFTSCARF_FFTPLANSINGLE
        typedef FFTPlanSingleOoura FFTPlanSingle;
    #endif
#elif (FFTSCARF_PRECISION_DEFAULT == 64)
    typedef FFTPlanOoura FFTPlanDoubleOoura;
    #ifndef FFTSCARF_FFTPLANDOUBLE
        #define FFTSCARF_FFTPLANDOUBLE
        typedef FFTPlanDoubleOoura FFTPlanDouble;
    #endif
#elif (FFTSCARF_PRECISION_DEFAULT == 128)
    typedef FFTPlanOoura FFTPlanLongDoubleOoura;
    #ifndef FFTSCARF_FFTPLANLONGDOUBLE
        #define FFTSCARF_FFTPLANLONGDOUBLE
        typedef FFTPlanLongDoubleOoura FFTPlanLongDouble;
    #endif
#endif

#ifndef FFTSCARF_FFTPLAN
    #define FFTSCARF_FFTPLAN
    typedef FFTPlanOoura FFTPlan;
#endif
}

#endif // FFTSCARF_FFT_OOURA

#endif // __FFTSCARF_OOURA_H__
