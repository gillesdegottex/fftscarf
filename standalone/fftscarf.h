/*! \file

How to call:
FFTPlanSingle
FFTPlanDouble
FFTPlan (will use the default precision given by FFTScarf (selected at FFTSarf compile time))
*/

#ifndef __FFTSCARF_H__
#define __FFTSCARF_H__

// =============================================================================
// You can comment out/in the following to activate/deactivate the
// various options and implementations options

#define FFTSCARF_PRECISION_SINGLE
#define FFTSCARF_PRECISION_DOUBLE
/* #undef FFTSCARF_PRECISION_LONGDOUBLE */
#define FFTSCARF_PRECISION_DEFAULT 32
/* #undef FFTSCARF_PLAN_PROTECTACCESS */

// #define FFTSCARF_FFT_IPP
// #define FFTSCARF_FFT_FFTS
// #define FFTSCARF_FFT_PFFFT
// #define FFTSCARF_FFT_FFTW3
#define FFTSCARF_FFT_OOURA
#define FFTSCARF_FFT_FFTREAL
/* #undef FFTSCARF_FFT_DFT */
/* #undef FFTSCARF_FFT_DJBFFT */

// =============================================================================

#define FFTSCARF_VERSION "1.5.4-21-g6cc045a"

#if FFTSCARF_PRECISION_DEFAULT == 32
    #undef OOFLOAT_SINGLE
    #define OOFLOAT_SINGLE
#elif FFTSCARF_PRECISION_DEFAULT == 64
    #undef OOFLOAT_DOUBLE
    #define OOFLOAT_DOUBLE
#elif FFTSCARF_PRECISION_DEFAULT == 128
    #undef OOFLOAT_LONGDOUBLE
    #define OOFLOAT_LONGDOUBLE
#endif

#ifdef FFTSCARF_PLAN_PROTECTACCESS
#define FFTSCARF_PLAN_ACCESS_DECLARE std::mutex m_plan_access;
#define FFTSCARF_PLAN_ACCESS_LOCK m_plan_access.lock();
#define FFTSCARF_PLAN_ACCESS_UNLOCK m_plan_access.unlock();
#else
#define FFTSCARF_PLAN_ACCESS_DECLARE
#define FFTSCARF_PLAN_ACCESS_LOCK
#define FFTSCARF_PLAN_ACCESS_UNLOCK
#endif

// Check if GPL is enforced on the user's software
#ifdef FFTSCARF_FFT_FFTW3
    // If the FFTW3 is used, the GPL is enforced on the software.
    #define FFTSCARF_LICENSE_GPLENFORCED
#endif

#include <assert.h>
#include <string>
#include <map>
#include <list>
#include <limits>
#include <complex>
#include <algorithm>
#include <cmath>
#ifdef FFTSCARF_PLAN_PROTECTACCESS
    #include <mutex>
#endif
#include <iostream>

/*! \brief General namespace of the wrapper library
 */
namespace fftscarf {
    //! Return the version of FFTScarf as a string.
    static inline std::string version(){
        std::stringstream ver;
        // ver << FFTSCARF_VERSION_MAJOR << "." << FFTSCARF_VERSION_MINOR << "." << FFTSCARF_VERSION_REVISION;
        ver << FFTSCARF_VERSION;
        return ver.str();
    }

    //! Return the machine epsilon, that is, the difference between 1.0 and the next value representable by the floating-point type FloatType.
    template<typename FloatType>
    FloatType eps() {return std::numeric_limits<FloatType>::epsilon();}

    //! The pi value with the highest precision for a standard C++ float type, i.e. long double.
    static const long double pi = 3.141592653589793238462643383279502884L;
    static const long double pi2 = 2*3.141592653589793238462643383279502884L;

    //! Return true if N is a power of 2
    static inline bool isPow2(unsigned int N){
        return ((N!=0) && !(N & (N-1)));
    }

    //! Return true if N is of the form 2^a * 3^b * 5^c
    static inline bool isPow235(unsigned int N){
        if(isPow2(N))
            return true;

        while(N%2==0) N /= 2;
        while(N%3==0) N /= 3;
        while(N%5==0) N /= 5;

        return N==1;
    }

    //! Return true if N is odd
    static inline bool isOdd(unsigned int N){
        return N&1;
    }

    //! Return true if N is even
    static inline bool isEven(unsigned int N){
        return !isOdd(N);
    }

    //! Wrap the phase value in [-pi, +pi]
    template<typename FloatType>
    inline FloatType wrap(FloatType value){
        // return std::arg(std::complex<FloatType>(std::cos(value),std::sin(value)));
        return std::atan2(std::sin(value),std::cos(value));
    }
    //! Wrap the phase value in [-pi, +pi] (quick version, a few eps different from wrap)
    template<typename FloatType>
    inline FloatType wrapq(FloatType value){
        double pi = fftscarf::pi;        // It seems double is used somewhere for all precision
        long double pi2 = fftscarf::pi2; // Should always use the highest available precision

        value = value - int(value/pi2)*pi2;

        if(value>pi)
            value -= pi2;
        else if(value<-pi)
            value += pi2;

        return value;
    }
    //! Given a reference phase value (in [-pi,+pi]) unwrap a second phase value to reduce factor of 2*pi distance (the second phase value is likely to end up outside of [-pi,+pi]).
    template<typename FloatType>
    void unwrap(FloatType phiref, FloatType& phi){
        if(phiref-phi>fftscarf::pi)
            phi += fftscarf::pi2;
        if(phi-phiref>fftscarf::pi)
            phi -= fftscarf::pi2;
    }

    //! Return a complex value from an array of two values.
    template<typename FloatType>
    inline std::complex<FloatType> make_complex(FloatType value[]){
        return std::complex<FloatType>(value[0], value[1]);
    }

    //! Return a complex value from the given real and imaginary parts.
    template<typename FloatType>
    inline std::complex<FloatType> make_complex(FloatType real, FloatType imag){
        return std::complex<FloatType>(real, imag);
    }

    /*! \brief Interface to be implemented by any FFT implementation
     * The basic idea isthe  to implement a "plan", which follows the same
     * definition as in FFTW3. Namely:
     * - A plan is either forward or backward (FFT or inverse FFT, respectively)
     * - It holds the trigonometric tables for computing an FFT of a given size.
     * - The size of the plan is not fixed and can be resized at run-time.
     * - The plan can be "executed" to tranform some given data by calling the member functions fft(.) or ifft(.)
     */
    class FFTPlanImplementation
    {
    protected:
        bool m_forward;
        int m_size = -1;

    public:
        //! Simple constructor. By default, the plan compute the forward FFT (i.e. not the inverse FFT)
        FFTPlanImplementation(bool forward=true){
            m_forward = forward;
        }
        //! A constructor for directly initializing the size of the FFT.
        FFTPlanImplementation(int n, bool forward=true){
            (void)n;
            m_forward = forward;
            // resize(n); // Don't call it here the vtable might not be ready. Let the child class deal with it.
        }
        virtual ~FFTPlanImplementation(){
        }

        //! Resize the FFT plan to the given size (size restrictions may apply depending on the implementation)
        virtual void resize(int n)=0;
        //! Return the current size of the FFT plan
        int size(){return m_size;}

        // Interface to follow for implementations:
        // (these are "ghost" functions, because static polymorphism doesn't exist)
        //! Return the version of the FFT implementation
        // static std::string version(){return "unknown FFT library version";}
        //! Return the name of the FFT implementation
        // static std::string libraryName(){return "unknown FFT library";}

        // To execute the DFT computation:
        // (these are "ghost" functions, because template polymorphism doesn't exist)
        //! Compute the forward FFT
        // template<typename TypeInContainer, typename TypeOutContainer>
        // void rfft(const TypeInContainer& in, TypeOutContainer& out, int dftlen=-1);
        //! Compute the inverse (backward) FFT
        // template<typename TypeInContainer, typename TypeOutContainer>
        // void irfft(const TypeInContainer& in, TypeOutContainer& out, int winlen=-1);
    };

    /*! \brief Manage a collection of DFT plans with different sizes.
     * It basically avoids re-initializing FFT plans.
     */
    template<typename _FFTPlanType>
    class FFTPlanManager
    {
    public:
        typedef _FFTPlanType FFTPlanType;
    private:
        bool m_forward;
        std::map<int,FFTPlanType*> m_plans;

    public:
        //! Build a plan manager for forward FFT or backward IFFT
        FFTPlanManager(bool forward=true){
            m_forward = forward;
        }
        ~FFTPlanManager(){
            for(size_t u=0; u<m_plans.size(); ++u)
                delete m_plans[u];
            m_plans.clear();
        }

        //! Return a plan of the given size
        inline FFTPlanType* plan(int dftlen){
            assert(dftlen>0);

            typename std::map<int,FFTPlanType*>::iterator dlit = m_plans.find(dftlen);
            if(dlit!=m_plans.end())
                return dlit->second;
            else
                return prepare(dftlen);
        }

        //! Prepare a plan with a specific size
        inline FFTPlanType* prepare(int dftlen){
            assert(dftlen>0);

            if(has(dftlen))
                return plan(dftlen);

            FFTPlanType* plan = new FFTPlanType(dftlen, m_forward);
            m_plans[dftlen] = plan;

            return plan;
        }
        //! Prepare plans of sizes between 2^p_min to 2^p_max
        inline void prepare_osf(int p_min, int p_max){
            for(int p=p_min; p<=p_max; ++p)
                prepare(std::pow(2, p));
        }

        inline bool has(int dftlen){
            assert(dftlen>0);
            return m_plans.find(dftlen)!=m_plans.end();
        }

        inline int shortest(){
            if(m_plans.size()==0) return -1;
            return m_plans.begin()->first;
        }

        inline int longest(){
            if(m_plans.size()==0) return -1;
            return m_plans.rbegin()->first;
        }

        //! Write some information about the plan manager
        void write_status(std::ostream& out){
            if(m_forward)
                out << "Forward FFT ";
            else
                out << "Backward IFFT ";
            out << "FFTPlanManager using " << FFTPlanType::libraryName();
            if(m_plans.empty())
                out << " without prepared sizes" << std::endl;
            else{
                out << " with sizes: ";
                for(typename std::map<int,FFTPlanType*>::iterator it=m_plans.begin(); it!=m_plans.end(); ++it)
                    out << it->first << " ";
                out << std::endl;
            }
        }

        //! Compute the forward FFT
        template<typename TypeInContainer, typename TypeOutContainer>
        inline void rfft(const TypeInContainer& in, TypeOutContainer& out, int dftlen){
            assert(dftlen>0);
            plan(dftlen)->rfft(in, out, dftlen);
        }

        //! Compute the inverse FFT (Currently limited to even sizes)
        template<typename TypeInContainer, typename TypeOutContainer>
        inline void irfft(const TypeInContainer& in, TypeOutContainer& out, int winlen=-1){
            assert(isOdd(in.size()));                    // TODO Works only for even sizes!
            plan((in.size()-1)*2)->irfft(in, out, winlen); // TODO Works only for even sizes!
        }
    };
}


// Make a macro that can be included in a cpp file instead of including the libfftscarf.a library

#ifdef FFTSCARF_PRECISION_SINGLE
#define FFTSCARF_CPP_SINGLE namespace fftscarf {FFTPlanManager<FFTPlanSingle> s_pm_plan_fwd_single(true); FFTPlanManager<FFTPlanSingle> s_pm_plan_bck_single(false);}
#else
#define FFTSCARF_CPP_SINGLE
#endif

#ifdef FFTSCARF_PRECISION_DOUBLE
#define FFTSCARF_CPP_DOUBLE namespace fftscarf {FFTPlanManager<FFTPlanDouble> s_pm_plan_fwd_double(true); FFTPlanManager<FFTPlanDouble> s_pm_plan_bck_double(false);}
#else
#define FFTSCARF_CPP_DOUBLE
#endif

#ifdef FFTSCARF_PRECISION_LONGDOUBLE
#define FFTSCARF_CPP_LONGDOUBLE namespace fftscarf {FFTPlanManager<FFTPlanLongDouble> s_pm_plan_fwd_long_double(true); FFTPlanManager<FFTPlanLongDouble> s_pm_plan_bck_long_double(false);}
#else
#define FFTSCARF_CPP_LONGDOUBLE
#endif

#ifdef FFTSCARF_FFT_IPP
#define FFTSCARF_CPP_IPPINIT bool fftscarf::FFTPlanIPP__s_ipp_initialized=false;
#else
#define FFTSCARF_CPP_IPPINIT
#endif

#define FFTSCARF_CPP FFTSCARF_CPP_SINGLE FFTSCARF_CPP_DOUBLE FFTSCARF_CPP_LONGDOUBLE FFTSCARF_CPP_IPPINIT

#endif // __FFTSCARF_H__

// Include files of the FFT Implementations are concatenated here below
#ifndef __FFTSCARF_IPP_H__
#define __FFTSCARF_IPP_H__

#ifdef FFTSCARF_FFT_IPP

#include <cassert>
#include <cmath>
#include <complex>
#include <vector>
#include <string>
#include <cmath>
#include <iostream>

#include <ipp.h>

namespace fftscarf {

#ifndef DOXYGEN_SHOULD_SKIP_THIS
extern bool FFTPlanIPP__s_ipp_initialized;
#endif /* DOXYGEN_SHOULD_SKIP_THIS */

#ifdef _WIN32
    #define FFTSCARF_FFT_IPP_CALLCONV __stdcall
#else
    #define FFTSCARF_FFT_IPP_CALLCONV
#endif

template<typename _FloatType, typename IppsFFTSpec_R_NNf, typename IppNNf, IppNNf* (FFTSCARF_FFT_IPP_CALLCONV *ippsMalloc_NNf)(int len), IppStatus (FFTSCARF_FFT_IPP_CALLCONV *ippsFFTGetSize_R_NNf)(int, int, IppHintAlgorithm, int*, int*, int*), IppStatus (FFTSCARF_FFT_IPP_CALLCONV *ippsFFTInit_R_NNf)(IppsFFTSpec_R_NNf**, int, int, IppHintAlgorithm, Ipp8u*, Ipp8u*),  IppStatus (FFTSCARF_FFT_IPP_CALLCONV *ippsFFTFwd_RToPerm_NNf)(const IppNNf*, IppNNf*, const IppsFFTSpec_R_NNf*, Ipp8u*), IppStatus (FFTSCARF_FFT_IPP_CALLCONV *ippsFFTInv_PermToR_NNf)(const IppNNf*, IppNNf*, const IppsFFTSpec_R_NNf*, Ipp8u*) >
class FFTPlanIPPTemplate : public FFTPlanImplementation
{
public:
    typedef _FloatType FloatType;

private:
    IppsFFTSpec_R_NNf *m_pFFTSpec;
    Ipp8u *m_pFFTSpecBuf;
    Ipp8u *m_pFFTWorkBuf;
    IppNNf *m_pSrc;
    IppNNf *m_pDst;
    FFTSCARF_PLAN_ACCESS_DECLARE

    void init(){
        if(!FFTPlanIPP__s_ipp_initialized){
            ippInit();
            FFTPlanIPP__s_ipp_initialized = true;
        }
        m_pFFTSpec = NULL;
        m_pFFTSpecBuf=NULL;
        m_pFFTWorkBuf=NULL;
        m_pSrc=NULL;
        m_pDst=NULL;
    }

public:
    static std::string version(){
        const IppLibraryVersion *lib = ippGetLibVersion();
        return std::string(lib->Name)+" "+std::string(lib->Version); // This is the current used version
    }
    static std::string libraryName(){
        std::stringstream result;
        result << std::string("Intel IPP ") << version() << " (precision " << 8*sizeof(FloatType) << "b)";
        return result.str();
    }

    FFTPlanIPPTemplate(bool forward=true)
        : FFTPlanImplementation(forward)
    {
        init();
    }
    FFTPlanIPPTemplate(int n, bool forward=true)
        : FFTPlanImplementation(n, forward)
    {
        init();

        resize(n);
    }

    void resize(int n)
    {
        if(n==m_size) return;

        assert(n>0);
        assert(isPow2(n));

        FFTSCARF_PLAN_ACCESS_LOCK
        m_size = n;

        const int order=(int)(log((double)m_size)/log(2.0));

        if(m_pSrc)  ippFree(m_pSrc);
        if(m_pDst)  ippFree(m_pDst);
        m_pSrc = ippsMalloc_NNf(m_size);
        m_pDst = ippsMalloc_NNf(m_size);

        // Query to get buffer sizes
        int sizeFFTSpecBuf;
        int sizeFFTInitBuf;
        int sizeFFTWorkBuf;
        ippsFFTGetSize_R_NNf(order, IPP_FFT_DIV_INV_BY_N, ippAlgHintNone, &sizeFFTSpecBuf, &sizeFFTInitBuf, &sizeFFTWorkBuf);

        // Alloc FFT buffers
        Ipp8u *pFFTInitBuf = ippsMalloc_8u(sizeFFTInitBuf);
        if(m_pFFTSpecBuf)   ippFree(m_pFFTSpecBuf);
        m_pFFTSpecBuf = ippsMalloc_8u(sizeFFTSpecBuf);
        if(m_pFFTWorkBuf)   ippFree(m_pFFTWorkBuf);
        m_pFFTWorkBuf = ippsMalloc_8u(sizeFFTWorkBuf);

        // Initialize FFT
        ippsFFTInit_R_NNf(&m_pFFTSpec, order, IPP_FFT_DIV_INV_BY_N, ippAlgHintNone, m_pFFTSpecBuf, pFFTInitBuf);

        if(pFFTInitBuf) ippFree(pFFTInitBuf);

        FFTSCARF_PLAN_ACCESS_UNLOCK
    }

    template<typename TypeInContainer, typename TypeOutContainer>
    void rfft(const TypeInContainer& in, TypeOutContainer& out, int dftlen=-1){
        if (!m_forward)
            throw std::string("A backward IDFT FFTPlan cannot compute the forward DFT");

        if(dftlen>0 && m_size!=dftlen)
            resize(dftlen);
        dftlen = m_size;

        // Fill pSrc
        size_t u = 0;
        for(; u<size_t(in.size()); ++u)
            m_pSrc[u] = in[u];
        for(; u<size_t(m_size); ++u)
            m_pSrc[u] = 0.0;

        // Do the FFT
        FFTSCARF_PLAN_ACCESS_LOCK
        ippsFFTFwd_RToPerm_NNf(m_pSrc, m_pDst, m_pFFTSpec, m_pFFTWorkBuf);
        FFTSCARF_PLAN_ACCESS_UNLOCK

        // Unpack pDst
        int neededoutsize = (m_size%2==1)?(m_size-1)/2+1:m_size/2+1;
        if(int(out.size())!=neededoutsize)
            out.resize(neededoutsize);

        out[0] = m_pDst[0]; // DC
        out[m_size/2] = m_pDst[1]; // Nyquist
        for(int f=1; f<m_size/2; f++)
            out[f] = make_complex(m_pDst[2*f], m_pDst[2*f+1]);
    }

    template<typename TypeInContainer, typename TypeOutContainer>
    void irfft(const TypeInContainer& in, TypeOutContainer& out, int winlen=-1){
        if(m_forward)
            throw std::string("A forward DFT FFTPlan cannot compute the backward IDFT");

        int dftlen = (in.size()-1)*2;
        if(m_size!=dftlen)
            resize(dftlen);

        if(winlen==-1)
            winlen = m_size;

        m_pSrc[0] = in[0].real(); // DC
        m_pSrc[1] = in[m_size/2].real(); // Nyquist
        for(int f=1; f<m_size/2; f++){
            m_pSrc[2*f] = in[f].real();
            m_pSrc[2*f+1] = in[f].imag();
        }

        // Do the inverse FFT
        FFTSCARF_PLAN_ACCESS_LOCK
        ippsFFTInv_PermToR_NNf(m_pSrc, m_pDst, m_pFFTSpec, m_pFFTWorkBuf);
        FFTSCARF_PLAN_ACCESS_UNLOCK

        // Unpack pDst
        if(int(out.size())!=winlen)
            out.resize(winlen);

        for(size_t u=0; u<size_t(winlen); ++u)
            out[u] = m_pDst[u];
    }

    ~FFTPlanIPPTemplate() {
        FFTSCARF_PLAN_ACCESS_LOCK
        if(m_pSrc)  ippFree(m_pSrc);
        if(m_pDst)  ippFree(m_pDst);
        if(m_pFFTSpecBuf)   ippFree(m_pFFTSpecBuf);
        if(m_pFFTWorkBuf)   ippFree(m_pFFTWorkBuf);
        FFTSCARF_PLAN_ACCESS_UNLOCK
    }
};

#ifdef FFTSCARF_PRECISION_SINGLE
    typedef FFTPlanIPPTemplate<float, IppsFFTSpec_R_32f, Ipp32f, ippsMalloc_32f, ippsFFTGetSize_R_32f, ippsFFTInit_R_32f, ippsFFTFwd_RToPerm_32f, ippsFFTInv_PermToR_32f > FFTPlanSingleIPP;
    #ifndef FFTSCARF_FFTPLANSINGLE
        #define FFTSCARF_FFTPLANSINGLE
        typedef FFTPlanSingleIPP FFTPlanSingle;
    #endif
#endif
#ifdef FFTSCARF_PRECISION_DOUBLE
    typedef FFTPlanIPPTemplate<double, IppsFFTSpec_R_64f, Ipp64f, ippsMalloc_64f, ippsFFTGetSize_R_64f, ippsFFTInit_R_64f, ippsFFTFwd_RToPerm_64f, ippsFFTInv_PermToR_64f > FFTPlanDoubleIPP;
    #ifndef FFTSCARF_FFTPLANDOUBLE
        #define FFTSCARF_FFTPLANDOUBLE
        typedef FFTPlanDoubleIPP FFTPlanDouble;
    #endif
#endif

#if FFTSCARF_PRECISION_DEFAULT == 32
    typedef FFTPlanSingleIPP FFTPlanIPP;
#elif FFTSCARF_PRECISION_DEFAULT == 64
    typedef FFTPlanDoubleIPP FFTPlanIPP;
#endif

#ifndef FFTSCARF_FFTPLAN
    #if FFTSCARF_PRECISION_DEFAULT == 32
        #define FFTSCARF_FFTPLAN
        typedef FFTPlanSingleIPP FFTPlan;
    #elif FFTSCARF_PRECISION_DEFAULT == 64
        #define FFTSCARF_FFTPLAN
        typedef FFTPlanDoubleIPP FFTPlan;
    #endif
#endif
}

#endif // FFTSCARF_FFT_IPP

#endif // __FFTSCARF_IPP_H__
/*

 This file is part of FFTS.

 Copyright (c) 2012, Anthony M. Blake
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 	* Redistributions of source code must retain the above copyright
 		notice, this list of conditions and the following disclaimer.
 	* Redistributions in binary form must reproduce the above copyright
 		notice, this list of conditions and the following disclaimer in the
 		documentation and/or other materials provided with the distribution.
 	* Neither the name of the organization nor the
	  names of its contributors may be used to endorse or promote products
 		derived from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL ANTHONY M. BLAKE BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef FFTS_H
#define FFTS_H

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

#if (defined(_WIN32) || defined(WIN32)) && defined(FFTS_SHARED)
#  ifdef FFTS_BUILD
#    define FFTS_API __declspec(dllexport)
#  else
#    define FFTS_API __declspec(dllimport)
#  endif
#else
#  if (__GNUC__ >= 4) || defined(HAVE_GCC_VISIBILITY)
#    define FFTS_API __attribute__ ((visibility("default")))
#  else
#    define FFTS_API
#  endif
#endif

/* The direction of the transform
   (i.e, the sign of the exponent in the transform.)
*/
#define FFTS_FORWARD (-1)
#define FFTS_BACKWARD (+1)

struct _ffts_plan_t;
typedef struct _ffts_plan_t ffts_plan_t;

/* Complex data is stored in the interleaved format
   (i.e, the real and imaginary parts composing each
   element of complex data are stored adjacently in memory)

   The multi-dimensional arrays passed are expected to be
   stored as a single contiguous block in row-major order
*/
FFTS_API ffts_plan_t*
ffts_init_1d(size_t N, int sign);

FFTS_API ffts_plan_t*
ffts_init_2d(size_t N1, size_t N2, int sign);

FFTS_API ffts_plan_t*
ffts_init_nd(int rank, size_t *Ns, int sign);

/* For real transforms, sign == FFTS_FORWARD implies a real-to-complex
   forwards tranform, and sign == FFTS_BACKWARD implies a complex-to-real
   backwards transform.

   The output of a real-to-complex transform is N/2+1 complex numbers,
   where the redundant outputs have been omitted.
*/
FFTS_API ffts_plan_t*
ffts_init_1d_real(size_t N, int sign);

FFTS_API ffts_plan_t*
ffts_init_2d_real(size_t N1, size_t N2, int sign);

FFTS_API ffts_plan_t*
ffts_init_nd_real(int rank, size_t *Ns, int sign);

FFTS_API void
ffts_execute(ffts_plan_t *p, const void *input, void *output);

FFTS_API void
ffts_free(ffts_plan_t *p);

#ifdef __cplusplus
}
#endif

#endif /* FFTS_H */
/*

 This file is part of FFTS -- The Fastest Fourier Transform in the South

 Copyright (c) 2012, Anthony M. Blake <amb@anthonix.com>
 Copyright (c) 2012, The University of Waikato

 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 	* Redistributions of source code must retain the above copyright
 		notice, this list of conditions and the following disclaimer.
 	* Redistributions in binary form must reproduce the above copyright
 		notice, this list of conditions and the following disclaimer in the
 		documentation and/or other materials provided with the distribution.
 	* Neither the name of the organization nor the
	  names of its contributors may be used to endorse or promote products
 		derived from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL ANTHONY M. BLAKE BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef FFTS_ATTRIBUTES_H
#define FFTS_ATTRIBUTES_H

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Macro definitions for various function/variable attributes */
#ifdef __GNUC__
#define GCC_VERSION_AT_LEAST(x,y) \
	(__GNUC__ > x || __GNUC__ == x && __GNUC_MINOR__ >= y)
#else
#define GCC_VERSION_AT_LEAST(x,y) 0
#endif

#ifdef __GNUC__
#define FFTS_ALIGN(x) __attribute__((aligned(x)))
#elif defined(_MSC_VER)
#define FFTS_ALIGN(x) __declspec(align(x))
#else
#define FFTS_ALIGN(x)
#endif

#if GCC_VERSION_AT_LEAST(3,1)
#define FFTS_ALWAYS_INLINE __attribute__((always_inline)) inline
#elif defined(_MSC_VER)
#define FFTS_ALWAYS_INLINE __forceinline
#else
#define FFTS_ALWAYS_INLINE inline
#endif

#if defined(_MSC_VER)
#define FFTS_INLINE __inline
#else
#define FFTS_INLINE inline
#endif

#if defined(__GNUC__)
#define FFTS_RESTRICT __restrict
#elif defined(_MSC_VER)
#define FFTS_RESTRICT __restrict
#else
#define FFTS_RESTRICT
#endif

#if GCC_VERSION_AT_LEAST(4,5)
#define FFTS_ASSUME(cond) do { if (!(cond)) __builtin_unreachable(); } while (0)
#elif defined(_MSC_VER)
#define FFTS_ASSUME(cond) __assume(cond)
#else
#define FFTS_ASSUME(cond)
#endif

#if GCC_VERSION_AT_LEAST(4,7)
#define FFTS_ASSUME_ALIGNED_16(x) __builtin_assume_aligned(x, 16)
#else
#define FFTS_ASSUME_ALIGNED_16(x) x
#endif

#if GCC_VERSION_AT_LEAST(4,7)
#define FFTS_ASSUME_ALIGNED_32(x) __builtin_assume_aligned(x, 32)
#else
#define FFTS_ASSUME_ALIGNED_32(x) x
#endif

#if defined(__GNUC__)
#define FFTS_LIKELY(cond) __builtin_expect(!!(cond), 1)
#else
#define FFTS_LIKELY(cond) cond
#endif

#if defined(__GNUC__)
#define FFTS_UNLIKELY(cond) __builtin_expect(!!(cond), 0)
#else
#define FFTS_UNLIKELY(cond) cond
#endif

#endif /* FFTS_ATTRIBUTES_H */
#ifndef __FFTSCARF_FFTS_H__
#define __FFTSCARF_FFTS_H__

#ifdef FFTSCARF_FFT_FFTS

#ifdef HAVE_SSE
#include <xmmintrin.h>
#endif
#include <stdlib.h>

#include <cmath>
#include <complex>
#include <vector>
#include <string>
#include <cmath>
#include <iostream>

//#include <ffts/include/ffts.h> // Will be included in fftscarf.h
//#include <ffts/src/ffts_attributes.h> // Will be included in fftscarf.h

namespace fftscarf {

class FFTPlanFFTS : public FFTPlanImplementation
{
public:
    typedef float FloatType;

private:
    FloatType FFTS_ALIGN(32) *m_signal;
    FloatType FFTS_ALIGN(32) *m_spec;
    ffts_plan_t *m_p;
    FFTSCARF_PLAN_ACCESS_DECLARE

public:
    static inline std::string version(){
        return std::string("0.9.0"); // This is the current built-in version
    }
    static inline std::string libraryName(){
        std::stringstream result;
        result << "FFTS " << version() << " (precision " << 8*sizeof(FloatType) << "b)"; // This is the current built-in version
        return result.str();
    }

    FFTPlanFFTS(bool forward=true)
        : FFTPlanImplementation(forward)
    {
        m_p = NULL;
        m_signal = NULL;
        m_spec = NULL;

        m_p = NULL;
    }
    FFTPlanFFTS(int n, bool forward=true)
        : FFTPlanImplementation(n, forward)
    {
        m_p = NULL;
        m_signal = NULL;
        m_spec = NULL;

        resize(n);
    }

    ~FFTPlanFFTS(){
        FFTSCARF_PLAN_ACCESS_LOCK

        if(m_signal || m_spec){
            #if (defined(_WIN32) || defined(WIN32))
                _aligned_free(m_signal);
                _aligned_free(m_spec);
            #else
                #ifdef HAVE_SSE
                    _mm_free(m_signal);
                    _mm_free(m_spec);
                #else
                    free(m_signal);
                    free(m_spec);
                #endif
            #endif
            m_signal = NULL;
            m_spec = NULL;
        }

        if(m_p){
            ffts_free(m_p);
            m_p = NULL;
        }

        FFTSCARF_PLAN_ACCESS_UNLOCK
    }

    virtual void resize(int n)
    {
        if(n==m_size) return;

        assert(n>0);
    //    assert(n<=65536);
        assert(isPow2(n));

        FFTSCARF_PLAN_ACCESS_LOCK
        m_size = n;

        if(m_signal || m_spec){
            #ifdef HAVE_SSE
                _mm_free(m_signal);
                _mm_free(m_spec);
            #else
                free(m_signal);
                free(m_spec);
            #endif
            m_signal = NULL;
            m_spec = NULL;
        }

        #if (defined(_WIN32) || defined(WIN32))
            m_signal = (FloatType FFTS_ALIGN(32) *) _aligned_malloc(2 * m_size * sizeof(FloatType), 32);
            m_spec = (FloatType FFTS_ALIGN(32) *) _aligned_malloc(2 * m_size * sizeof(FloatType), 32);
        #else
            // See http://www.delorie.com/gnu/docs/glibc/libc_31.html
            // Or ffts/tests/test.c
            #ifdef HAVE_SSE
                m_signal = (FloatType FFTS_ALIGN(32) *) _mm_malloc(2 * m_size * sizeof(FloatType), 32);
                m_spec = (FloatType FFTS_ALIGN(32) *) _mm_malloc(2 * m_size * sizeof(FloatType), 32);
            #else
                m_signal = (FloatType FFTS_ALIGN(32) *) valloc(2 * m_size * sizeof(FloatType));
                m_spec = (FloatType FFTS_ALIGN(32) *) valloc(2 * m_size * sizeof(FloatType));
            #endif
        #endif

        if(m_p){
            ffts_free(m_p);
            m_p = NULL;
        }

        if(m_forward)
            m_p = ffts_init_1d_real(n, FFTS_FORWARD); // TODO sign for the backward I suppose
        else
            m_p = ffts_init_1d_real(n, FFTS_BACKWARD); // TODO sign for the backward I suppose

        FFTSCARF_PLAN_ACCESS_UNLOCK
    }

    template<typename TypeInContainer, typename TypeOutContainer>
    void rfft(const TypeInContainer& in, TypeOutContainer& out, int dftlen=-1){
        if (!m_forward)
            throw std::string("A backward IDFT FFTPlan cannot compute the forward DFT");

        if(dftlen>0 && m_size!=dftlen)
            resize(dftlen);

        dftlen = m_size;

        int neededoutsize = (m_size%2==1)?(m_size-1)/2+1:m_size/2+1;
        if(int(out.size())!=neededoutsize)
            out.resize(neededoutsize);

        int u = 0;
        for(; u<int(in.size()); ++u)
            m_signal[u] = in[u];
        for(; u<dftlen; ++u)
            m_signal[u] = 0.0;

        FFTSCARF_PLAN_ACCESS_LOCK
        ffts_execute(m_p, m_signal, m_spec);
        FFTSCARF_PLAN_ACCESS_UNLOCK

        for(int f=0; f<m_size/2+1; f++)
            out[f] = make_complex(m_spec[2*f], m_spec[2*f+1]);
    }

    template<typename TypeInContainer, typename TypeOutContainer>
    void irfft(const TypeInContainer& in, TypeOutContainer& out, int winlen=-1){
        if(m_forward)
            throw std::string("A forward DFT FFTPlan cannot compute the backward IDFT");

        int dftlen = (in.size()-1)*2;
        if(m_size!=dftlen)
            resize(dftlen);

        if(winlen==-1)
            winlen = m_size;

        if(int(out.size())!=winlen)
            out.resize(winlen);

        for(int f=0; f<m_size/2+1; f++){
            m_spec[2*f] = in[f].real();
            m_spec[2*f+1] = in[f].imag();
        }

        FFTSCARF_PLAN_ACCESS_LOCK
        ffts_execute(m_p, m_spec, m_signal);
        FFTSCARF_PLAN_ACCESS_UNLOCK

        FloatType oneoverdftlen = FloatType(1.0)/m_size;
        for(int u=0; u<winlen; ++u)
            out[u] = m_signal[u]*oneoverdftlen;
    }
};

typedef FFTPlanFFTS FFTPlanSingleFFTS;
#ifndef FFTSCARF_FFTPLANSINGLE
    #define FFTSCARF_FFTPLANSINGLE
    typedef FFTPlanSingleFFTS FFTPlanSingle;
#endif
#ifndef FFTSCARF_FFTPLAN
    #define FFTSCARF_FFTPLAN
    typedef FFTPlanFFTS FFTPlan;
#endif
}

#endif // FFTSCARF_FFT_FFTS

#endif // __FFTSCARF_FFTS_H__
/* Copyright (c) 2013  Julien Pommier ( pommier@modartt.com )

   Based on original fortran 77 code from FFTPACKv4 from NETLIB,
   authored by Dr Paul Swarztrauber of NCAR, in 1985.

   As confirmed by the NCAR fftpack software curators, the following
   FFTPACKv5 license applies to FFTPACKv4 sources. My changes are
   released under the same terms.

   FFTPACK license:

   http://www.cisl.ucar.edu/css/software/fftpack5/ftpk.html

   Copyright (c) 2004 the University Corporation for Atmospheric
   Research ("UCAR"). All rights reserved. Developed by NCAR's
   Computational and Information Systems Laboratory, UCAR,
   www.cisl.ucar.edu.

   Redistribution and use of the Software in source and binary forms,
   with or without modification, is permitted provided that the
   following conditions are met:

   - Neither the names of NCAR's Computational and Information Systems
   Laboratory, the University Corporation for Atmospheric Research,
   nor the names of its sponsors or contributors may be used to
   endorse or promote products derived from this Software without
   specific prior written permission.

   - Redistributions of source code must retain the above copyright
   notices, this list of conditions, and the disclaimer below.

   - Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions, and the disclaimer below in the
   documentation and/or other materials provided with the
   distribution.

   THIS SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
   EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO THE WARRANTIES OF
   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
   NONINFRINGEMENT. IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT
   HOLDERS BE LIABLE FOR ANY CLAIM, INDIRECT, INCIDENTAL, SPECIAL,
   EXEMPLARY, OR CONSEQUENTIAL DAMAGES OR OTHER LIABILITY, WHETHER IN AN
   ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE
   SOFTWARE.
*/

/*
   PFFFT : a Pretty Fast FFT.

   This is basically an adaptation of the single precision fftpack
   (v4) as found on netlib taking advantage of SIMD instruction found
   on cpus such as intel x86 (SSE1), powerpc (Altivec), and arm (NEON).

   For architectures where no SIMD instruction is available, the code
   falls back to a scalar version.

   Restrictions:

   - 1D transforms only, with 32-bit single precision.

   - supports only transforms for inputs of length N of the form
   N=(2^a)*(3^b)*(5^c), a >= 5, b >=0, c >= 0 (32, 48, 64, 96, 128,
   144, 160, etc are all acceptable lengths). Performance is best for
   128<=N<=8192.

   - all (float*) pointers in the functions below are expected to
   have an "simd-compatible" alignment, that is 16 bytes on x86 and
   powerpc CPUs.

   You can allocate such buffers with the functions
   pffft_aligned_malloc / pffft_aligned_free (or with stuff like
   posix_memalign..)

*/

#ifndef PFFFT_H
#define PFFFT_H

#include <stddef.h> // for size_t

#ifdef __cplusplus
extern "C" {
#endif

  /* opaque struct holding internal stuff (precomputed twiddle factors)
     this struct can be shared by many threads as it contains only
     read-only data.
  */
  typedef struct PFFFT_Setup PFFFT_Setup;

  /* direction of the transform */
  typedef enum { PFFFT_FORWARD, PFFFT_BACKWARD } pffft_direction_t;

  /* type of transform */
  typedef enum { PFFFT_REAL, PFFFT_COMPLEX } pffft_transform_t;

  /*
    prepare for performing transforms of size N -- the returned
    PFFFT_Setup structure is read-only so it can safely be shared by
    multiple concurrent threads.
  */
  PFFFT_Setup *pffft_new_setup(int N, pffft_transform_t transform);
  void pffft_destroy_setup(PFFFT_Setup *);
  /*
     Perform a Fourier transform , The z-domain data is stored in the
     most efficient order for transforming it back, or using it for
     convolution. If you need to have its content sorted in the
     "usual" way, that is as an array of interleaved complex numbers,
     either use pffft_transform_ordered , or call pffft_zreorder after
     the forward fft, and before the backward fft.

     Transforms are not scaled: PFFFT_BACKWARD(PFFFT_FORWARD(x)) = N*x.
     Typically you will want to scale the backward transform by 1/N.

     The 'work' pointer should point to an area of N (2*N for complex
     fft) floats, properly aligned. If 'work' is NULL, then stack will
     be used instead (this is probably the best strategy for small
     FFTs, say for N < 16384).

     input and output may alias.
  */
  void pffft_transform(PFFFT_Setup *setup, const float *input, float *output, float *work, pffft_direction_t direction);

  /*
     Similar to pffft_transform, but makes sure that the output is
     ordered as expected (interleaved complex numbers).  This is
     similar to calling pffft_transform and then pffft_zreorder.

     input and output may alias.
  */
  void pffft_transform_ordered(PFFFT_Setup *setup, const float *input, float *output, float *work, pffft_direction_t direction);

  /*
     call pffft_zreorder(.., PFFFT_FORWARD) after pffft_transform(...,
     PFFFT_FORWARD) if you want to have the frequency components in
     the correct "canonical" order, as interleaved complex numbers.

     (for real transforms, both 0-frequency and half frequency
     components, which are real, are assembled in the first entry as
     F(0)+i*F(n/2+1). Note that the original fftpack did place
     F(n/2+1) at the end of the arrays).

     input and output should not alias.
  */
  void pffft_zreorder(PFFFT_Setup *setup, const float *input, float *output, pffft_direction_t direction);

  /*
     Perform a multiplication of the frequency components of dft_a and
     dft_b and accumulate them into dft_ab. The arrays should have
     been obtained with pffft_transform(.., PFFFT_FORWARD) and should
     *not* have been reordered with pffft_zreorder (otherwise just
     perform the operation yourself as the dft coefs are stored as
     interleaved complex numbers).

     the operation performed is: dft_ab += (dft_a * fdt_b)*scaling

     The dft_a, dft_b and dft_ab pointers may alias.
  */
  void pffft_zconvolve_accumulate(PFFFT_Setup *setup, const float *dft_a, const float *dft_b, float *dft_ab, float scaling);

  /*
    the float buffers must have the correct alignment (16-byte boundary
    on intel and powerpc). This function may be used to obtain such
    correctly aligned buffers.
  */
  void *pffft_aligned_malloc(size_t nb_bytes);
  void pffft_aligned_free(void *);

  /* return 4 or 1 wether support SSE/Altivec instructions was enable when building pffft.c */
  int pffft_simd_size();

#ifdef __cplusplus
}
#endif

#endif // PFFFT_H
#ifndef __FFTSCARF_FFT_PFFFT_H__
#define __FFTSCARF_FFT_PFFFT_H__

#ifdef FFTSCARF_FFT_PFFFT

#include <cmath>
#include <complex>
#include <vector>
#include <string>
#include <cmath>
#include <iostream>

//#include <pffft/pffft.h> // Will be included in fftscarf.h

namespace fftscarf {

class FFTPlanPFFFT : public FFTPlanImplementation
{
public:
    typedef float FloatType;

private:
    PFFFT_Setup *m_setup;
    FloatType *m_input;
    FloatType *m_output;
    FloatType *m_work;
    FFTSCARF_PLAN_ACCESS_DECLARE

public:
    static inline std::string version(){
        return std::string("2014-08-10"); // This is the current built-in version
    }
    static inline std::string libraryName(){
        std::stringstream result;
        result << "Pretty Fast FFT (PFFFT) " << version() << " (SIMD size " << pffft_simd_size() << ")" << " (precision " << 8*sizeof(FloatType) << "b)"; // This is the current built-in version
        return result.str();
    }

    FFTPlanPFFFT(bool forward=true)
        : FFTPlanImplementation(forward)
    {
        m_setup = NULL;
        m_input = NULL;
        m_output = NULL;
        m_work = NULL;
    }
    FFTPlanPFFFT(int n, bool forward=true)
        : FFTPlanImplementation(n, forward)
    {
        m_setup = NULL;
        m_input = NULL;
        m_output = NULL;
        m_work = NULL;

        resize(n);
    }
    ~FFTPlanPFFFT()
    {
        FFTSCARF_PLAN_ACCESS_LOCK
        if(m_setup || m_input || m_output){
            pffft_destroy_setup(m_setup);
            m_setup = NULL;
            pffft_aligned_free(m_input);
            m_input = NULL;
            pffft_aligned_free(m_output);
            m_output = NULL;
        }
        if(m_work){
            pffft_aligned_free(m_work);
            m_work = NULL;
        }
        FFTSCARF_PLAN_ACCESS_UNLOCK
    }

    virtual void resize(int n)
    {
        if(n==m_size) return;

        assert(n>0);
        assert(n>=32);
        assert(isPow235(n));

        FFTSCARF_PLAN_ACCESS_LOCK
        m_size = n;

        if(m_setup || m_input || m_output){
            pffft_destroy_setup(m_setup);
            m_setup = NULL;
            pffft_aligned_free(m_input);
            m_input = NULL;
            pffft_aligned_free(m_output);
            m_output = NULL;
        }
        if(m_work){
            pffft_aligned_free(m_work);
            m_work = NULL;
        }

        m_setup = pffft_new_setup(n, PFFFT_REAL);

        m_input = (FloatType*)pffft_aligned_malloc(n*sizeof(FloatType));
        m_output = (FloatType*)pffft_aligned_malloc(n*sizeof(FloatType));

        // Following the PFFFT's documentation
        if(n>=16384)
            m_work = (FloatType*)pffft_aligned_malloc(n*sizeof(FloatType));

        FFTSCARF_PLAN_ACCESS_UNLOCK
    }

    template<typename TypeInContainer, typename TypeOutContainer>
    void rfft(const TypeInContainer& in, TypeOutContainer& out, int dftlen=-1){
        if (!m_forward)
            throw std::string("A backward IDFT FFTPlan cannot compute the forward DFT");

        if(dftlen>0 && m_size!=dftlen)
            resize(dftlen);

        dftlen = m_size;

        int neededoutsize = (m_size%2==1)?(m_size-1)/2+1:m_size/2+1;
        if(int(out.size())!=neededoutsize)
            out.resize(neededoutsize);

        int u = 0;
        for(; u<int(in.size()); ++u)
            m_input[u] = in[u];
        for(; u<dftlen; ++u)
            m_input[u] = 0.0;

        FFTSCARF_PLAN_ACCESS_LOCK
        pffft_transform_ordered(m_setup, m_input, m_output, m_work, PFFFT_FORWARD);
        FFTSCARF_PLAN_ACCESS_UNLOCK

        out[0] = m_output[0]; // DC
        for(int f=1; f<m_size/2; f++)
            out[f] = make_complex(m_output[2*f], m_output[2*f+1]);
        out[m_size/2] = m_output[1]; // Nyquist
    }

    template<typename TypeInContainer, typename TypeOutContainer>
    void irfft(const TypeInContainer& in, TypeOutContainer& out, int winlen=-1){
        if(m_forward)
            throw std::string("A forward DFT FFTPlan cannot compute the backward IDFT");

        int dftlen = (in.size()-1)*2;
        if(m_size!=dftlen)
            resize(dftlen);

        if(winlen==-1)
            winlen = m_size;

        if(int(out.size())!=winlen)
            out.resize(winlen);

        m_input[0] = in[0].real(); // DC
        m_input[1] = in[m_size/2].real(); // Nyquist
        for(int f=1; f<m_size/2; f++){
            m_input[2*f] = in[f].real();
            m_input[2*f+1] = in[f].imag();
        }

        FFTSCARF_PLAN_ACCESS_LOCK
        pffft_transform_ordered(m_setup, m_input, m_output, m_work, PFFFT_BACKWARD);
        FFTSCARF_PLAN_ACCESS_UNLOCK

        FloatType oneoverdftlen = FloatType(1.0)/m_size;
        for(int u=0; u<winlen; ++u)
            out[u] = m_output[u]*oneoverdftlen;
    }
};

typedef FFTPlanPFFFT FFTPlanSinglePFFFT;
#ifndef FFTSCARF_FFTPLANSINGLE
    #define FFTSCARF_FFTPLANSINGLE
    typedef FFTPlanSinglePFFFT FFTPlanSingle;
#endif
#ifndef FFTSCARF_FFTPLAN
    #define FFTSCARF_FFTPLAN
    typedef FFTPlanPFFFT FFTPlan;
#endif
}

#endif // FFTSCARF_FFT_PFFFT

#endif // __FFTSCARF_FFT_PFFFT_H__
#ifndef __FFTSCARF_FFTW3_H__
#define __FFTSCARF_FFTW3_H__

#ifdef FFTSCARF_FFT_FFTW3

#include <cassert>
#include <cmath>
#include <complex>
#include <vector>
#include <string>
#include <cmath>
#include <iostream>

#include <fftw3.h>

namespace fftscarf {

template<typename _FloatType, typename fftwg_plan, typename fftwg_complex, fftwg_plan (*fftwg_plan_dft_r2c_1d)(int, _FloatType*, fftwg_complex*, unsigned), fftwg_plan (*fftwg_plan_dft_c2r_1d)(int, fftwg_complex*, _FloatType*, unsigned), void (*fftwg_execute)(const fftwg_plan), void (*fftwg_destroy_plan)(fftwg_plan), void* (*fftwg_malloc)(size_t), void (*fftwg_free)(void*)>
class FFTPlanFFTW3Template : public FFTPlanImplementation
{
public:
    typedef _FloatType FloatType;

private:
    fftwg_plan m_fftw3_plan;
    FloatType *m_fftw3_sig;
    fftwg_complex *m_fftw3_spec;
    FFTSCARF_PLAN_ACCESS_DECLARE

public:
    static std::string version(){
        return std::string("3");
    }
    static std::string libraryName(){
        std::stringstream result;
        result << std::string("FFTW ") << version() << " (precision " << 8*sizeof(FloatType) << "b)";
        return result.str();
    }

    #ifdef FFTW3RESIZINGMAXTIMESPENT
    static void setTimeLimitForPlanPreparation(float t){
        fftwg_set_timelimit(t); // From FFTW 3.1 only, no means to check version at compile time...
    }
    #endif

    FFTPlanFFTW3Template(bool forward=true) {
        m_size = -1;
        m_forward = forward;

        m_fftw3_plan = NULL;
        m_fftw3_sig = NULL;
        m_fftw3_spec = NULL;
    //    #ifdef FFTW3RESIZINGMAXTIMESPENT
    //        fftwg_set_timelimit(1.0); // From FFTW 3.1. No means exist to check version at compile time ...
    //    #endif
    }
    FFTPlanFFTW3Template(int n, bool forward=true) {
        m_size = -1;
        m_forward = forward;

        m_fftw3_plan = NULL;
        m_fftw3_sig = NULL;
        m_fftw3_spec = NULL;

        resize(n);
    }

    void resize(int n) {
        assert(n>0);

        if(n==m_size) return;

        FFTSCARF_PLAN_ACCESS_LOCK
        m_size = n;

        if(m_fftw3_sig)
            fftwg_free((void*)m_fftw3_sig);
        m_fftw3_sig = NULL;
        if(m_fftw3_spec)
            fftwg_free((void*)m_fftw3_spec);
        m_fftw3_spec = NULL;

        m_fftw3_sig = (FloatType*) fftwg_malloc(sizeof(FloatType) * m_size);
        m_fftw3_spec = (fftwg_complex*) fftwg_malloc(sizeof(fftwg_complex) * m_size);
        //  | FFTW_PRESERVE_INPUT
    //         unsigned int flags = FFTW_ESTIMATE;
        unsigned int flags = FFTW_ESTIMATE | FFTW_PRESERVE_INPUT;
        // The following is likely to generate non-deterministic runs !
        // See: http://www.fftw.org/faq/section3.html#nondeterministic
        // unsigned int flags = FFTW_MEASURE;
        if(m_forward){
            m_fftw3_plan = fftwg_plan_dft_r2c_1d(m_size, m_fftw3_sig, m_fftw3_spec, flags);
    //            m_fftw3_plan = fftw_plan_dft_1d(m_size, m_fftw3_sig, m_fftw3_spec, FFTW_FORWARD, FFTW_MEASURE);
        }
        else{
            m_fftw3_plan = fftwg_plan_dft_c2r_1d(m_size, m_fftw3_spec, m_fftw3_sig, flags);
    //            m_fftw3_plan = fftw_plan_dft_1d(m_size, m_fftw3_sig, m_fftw3_spec, FFTW_BACKWARD, FFTW_MEASURE);
        }
        FFTSCARF_PLAN_ACCESS_UNLOCK
    }

    template<typename TypeInContainer, typename TypeOutContainer>
    void rfft(const TypeInContainer& in, TypeOutContainer& out, int dftlen=-1) {
        if (!m_forward)
            throw std::string("A backward IDFT FFTPlan cannot compute the forward DFT");

        if(dftlen>0 && m_size!=dftlen)
            resize(dftlen);

        dftlen = m_size;

        int neededoutsize = (m_size%2==1)?(m_size-1)/2+1:m_size/2+1;
        if(int(out.size())!=neededoutsize)
            out.resize(neededoutsize);


        int u = 0;
        for(; u<int(in.size()); ++u)
            m_fftw3_sig[u] = in[u];
        for(; u<dftlen; ++u)
            m_fftw3_sig[u] = 0.0;

        FFTSCARF_PLAN_ACCESS_LOCK
        fftwg_execute(m_fftw3_plan);
        FFTSCARF_PLAN_ACCESS_UNLOCK

        for(size_t i=0; i<out.size(); ++i)
            out[i] = make_complex(m_fftw3_spec[i]);
    }

    template<typename TypeInContainer, typename TypeOutContainer>
    void irfft(const TypeInContainer& in, TypeOutContainer& out, int winlen=-1) {
        if(m_forward)
            throw std::string("A forward DFT FFTPlan cannot compute the backward IDFT");

        int dftlen = int((in.size()-1)*2);
        if(m_size!=dftlen)
            resize(dftlen);

        if(winlen==-1)
            winlen = m_size;

        if(int(out.size())!=winlen)
            out.resize(winlen);

        for(int i=0; i<m_size/2+1; i++){
            m_fftw3_spec[i][0] = in[i].real();
            m_fftw3_spec[i][1] = in[i].imag();
        }
        FFTSCARF_PLAN_ACCESS_LOCK
        fftwg_execute(m_fftw3_plan);
        FFTSCARF_PLAN_ACCESS_UNLOCK

        FloatType oneoverdftlen = FloatType(1.0)/m_size;
        for(int i=0; i<winlen; i++)
            out[i] = m_fftw3_sig[i]*oneoverdftlen;
    }

    ~FFTPlanFFTW3Template() {
        FFTSCARF_PLAN_ACCESS_LOCK
        if(m_fftw3_plan) fftwg_destroy_plan(m_fftw3_plan);
        if(m_fftw3_sig) fftwg_free((void*)m_fftw3_sig);
        if(m_fftw3_spec) fftwg_free((void*)m_fftw3_spec);
        FFTSCARF_PLAN_ACCESS_UNLOCK
    }
};

#ifdef FFTSCARF_PRECISION_SINGLE
    typedef FFTPlanFFTW3Template<float, fftwf_plan, fftwf_complex, fftwf_plan_dft_r2c_1d, fftwf_plan_dft_c2r_1d, fftwf_execute, fftwf_destroy_plan, fftwf_malloc, fftwf_free> FFTPlanSingleFFTW3;
    #ifndef FFTSCARF_FFTPLANSINGLE
        #define FFTSCARF_FFTPLANSINGLE
        typedef FFTPlanSingleFFTW3 FFTPlanSingle;
    #endif
#endif
#ifdef FFTSCARF_PRECISION_DOUBLE
    typedef FFTPlanFFTW3Template<double, fftw_plan, fftw_complex, fftw_plan_dft_r2c_1d, fftw_plan_dft_c2r_1d, fftw_execute, fftw_destroy_plan, fftw_malloc, fftw_free> FFTPlanDoubleFFTW3;
    #ifndef FFTSCARF_FFTPLANDOUBLE
        #define FFTSCARF_FFTPLANDOUBLE
        typedef FFTPlanDoubleFFTW3 FFTPlanDouble;
    #endif
#endif
#ifdef FFTSCARF_PRECISION_LONGDOUBLE
    typedef FFTPlanFFTW3Template<long double, fftwl_plan, fftwl_complex, fftwl_plan_dft_r2c_1d, fftwl_plan_dft_c2r_1d, fftwl_execute, fftwl_destroy_plan, fftwl_malloc, fftwl_free> FFTPlanLongDoubleFFTW3;
    #ifndef FFTSCARF_FFTPLANLONGDOUBLE
        #define FFTSCARF_FFTPLANLONGDOUBLE
        typedef FFTPlanLongDoubleFFTW3 FFTPlanLongDouble;
    #endif
#endif

#if FFTSCARF_PRECISION_DEFAULT == 32
    typedef FFTPlanSingleFFTW3 FFTPlanFFTW3;
#elif FFTSCARF_PRECISION_DEFAULT == 64
    typedef FFTPlanDoubleFFTW3 FFTPlanFFTW3;
#elif FFTSCARF_PRECISION_DEFAULT == 128
    typedef FFTPlanLongDoubleFFTW3 FFTPlanFFTW3;
#endif

#ifndef FFTSCARF_FFTPLAN
    #if FFTSCARF_PRECISION_DEFAULT == 32
        #define FFTSCARF_FFTPLAN
        typedef FFTPlanSingleFFTW3 FFTPlan;
    #elif FFTSCARF_PRECISION_DEFAULT == 64
        #define FFTSCARF_FFTPLAN
        typedef FFTPlanDoubleFFTW3 FFTPlan;
    #elif FFTSCARF_PRECISION_DEFAULT == 128
        #define FFTSCARF_FFTPLAN
        typedef FFTPlanLongDoubleFFTW3 FFTPlan;
    #endif
#endif
}

#endif // FFTSCARF_FFT_FFTW3

#endif // __FFTSCARF_FFTW3_H__
#ifndef __OOURA_H__
#define __OOURA_H__

#ifdef __cplusplus
extern "C" {
#endif

#if defined(OOFLOAT_SINGLE)
    #define OOFLOAT float
#elif defined(OOFLOAT_DOUBLE)
    #define OOFLOAT double
#elif defined(OOFLOAT_LONGDOUBLE)
    #define OOFLOAT long double
#endif

void cdft(int, int, OOFLOAT *, int *, OOFLOAT *);
void rdft(int, int, OOFLOAT *, int *, OOFLOAT *);
void ddct(int, int, OOFLOAT *, int *, OOFLOAT *);
void ddst(int, int, OOFLOAT *, int *, OOFLOAT *);
void dfct(int, OOFLOAT *, OOFLOAT *, int *, OOFLOAT *);
void dfst(int, OOFLOAT *, OOFLOAT *, int *, OOFLOAT *);

#ifdef __cplusplus
}
#endif

#endif // __OOURA_H__
#ifndef __FFTSCARF_OOURA_H__
#define __FFTSCARF_OOURA_H__

#ifdef FFTSCARF_FFT_OOURA

#include <cmath>
#include <complex>
#include <vector>
#include <string>

//extern "C" {
//#include <ooura/fftsg.h> // Will be included in fftscarf.h
//}

namespace fftscarf {

class FFTPlanOoura : public FFTPlanImplementation
{
public:
    typedef OOFLOAT FloatType;

private:
    FloatType* m_ooura_a = nullptr;
    int* m_ooura_ip = nullptr;
    FloatType* m_ooura_w = nullptr;

    FFTSCARF_PLAN_ACCESS_DECLARE

    inline void clear(){
        if(m_ooura_a){
            delete[] m_ooura_a;
            m_ooura_a = nullptr;
        }
        if(m_ooura_ip){
            delete[] m_ooura_ip;
            m_ooura_ip = nullptr;
        }
        if(m_ooura_w){
            delete[] m_ooura_w;
            m_ooura_w = nullptr;
        }
    }


public:
    static inline std::string version(){
        return std::string("2006.12"); // This is the current built-in version
    }
    static inline std::string libraryName(){
        std::stringstream result;
        result << "Ooura " << version() << " (precision " << 8*sizeof(FloatType) << "b)"; // This is the current built-in version
        return result.str();
    }

    FFTPlanOoura(bool forward=true)
        : FFTPlanImplementation(forward)
    {
    }
    FFTPlanOoura(int n, bool forward=true)
        : FFTPlanImplementation(n, forward)
    {
        resize(n);
    }

    virtual ~FFTPlanOoura(){
        FFTSCARF_PLAN_ACCESS_LOCK
        clear();
        FFTSCARF_PLAN_ACCESS_UNLOCK
    }

    virtual void resize(int n)
    {
        if(n==m_size) return;

        assert(n>0);
        assert(isPow2(n));

        FFTSCARF_PLAN_ACCESS_LOCK
        m_size = n;

        clear();

        m_ooura_a = new FloatType[m_size];
        m_ooura_ip = new int[2+(1<<(int)(std::log(m_size/2+0.5)/std::log(2.0))/2)];
        m_ooura_w = new FloatType[m_size/2];
        m_ooura_ip[0] = 0; // first time only
        rdft(m_size, 1, m_ooura_a, m_ooura_ip, m_ooura_w); // init cos/sin table

        FFTSCARF_PLAN_ACCESS_UNLOCK
    }

    template<typename TypeInContainer, typename TypeOutContainer>
    void rfft(const TypeInContainer& in, TypeOutContainer& out, int dftlen=-1){
        if (!m_forward)
            throw std::string("A backward IDFT FFTPlan cannot compute the forward DFT");

        if(dftlen>0 && m_size!=dftlen)
            resize(dftlen);

        dftlen = m_size;

        int neededoutsize = (m_size%2==1)?(m_size-1)/2+1:m_size/2+1;
        if(int(out.size())!=neededoutsize)
            out.resize(neededoutsize);

        int u = 0;
        for(; u<int(in.size()); ++u)
            m_ooura_a[u] = in[u];
        for(; u<dftlen; ++u)
            m_ooura_a[u] = 0.0;

        FFTSCARF_PLAN_ACCESS_LOCK
        rdft(m_size, 1, m_ooura_a, m_ooura_ip, m_ooura_w);
        FFTSCARF_PLAN_ACCESS_UNLOCK

        out[0] = m_ooura_a[0]; // DC
        for(int f=1; f<m_size/2; f++)
            out[f] = make_complex(m_ooura_a[2*f], -m_ooura_a[2*f+1]);
        out[m_size/2] = m_ooura_a[1]; // Nyquist
    }

    template<typename TypeInContainer, typename TypeOutContainer>
    void irfft(const TypeInContainer& in, TypeOutContainer& out, int winlen=-1){
        if(m_forward)
            throw std::string("A forward DFT FFTPlan cannot compute the backward IDFT");

        int dftlen = int((in.size()-1)*2);
        if(m_size!=dftlen)
            resize(dftlen);

        if(winlen==-1)
            winlen = m_size;

        if(int(out.size())!=winlen)
            out.resize(winlen);

        m_ooura_a[0] = in[0].real(); // DC
        for(int f=1; f<m_size/2; f++){
            m_ooura_a[2*f] = in[f].real();
            m_ooura_a[2*f+1] = -in[f].imag();
        }
        m_ooura_a[1] = in[m_size/2].real(); // Nyquist

        FFTSCARF_PLAN_ACCESS_LOCK
        rdft(m_size, -1, m_ooura_a, m_ooura_ip, m_ooura_w);
        FFTSCARF_PLAN_ACCESS_UNLOCK

        FloatType oneoverdftlen = FloatType(2.0)/m_size;
        for(int u=0; u<winlen; ++u)
            out[u] = m_ooura_a[u]*oneoverdftlen;
    }
};

#if (FFTSCARF_PRECISION_DEFAULT == 32)
    typedef FFTPlanOoura FFTPlanSingleOoura;
    #ifndef FFTSCARF_FFTPLANSINGLE
        #define FFTSCARF_FFTPLANSINGLE
        typedef FFTPlanSingleOoura FFTPlanSingle;
    #endif
#elif (FFTSCARF_PRECISION_DEFAULT == 64)
    typedef FFTPlanOoura FFTPlanDoubleOoura;
    #ifndef FFTSCARF_FFTPLANDOUBLE
        #define FFTSCARF_FFTPLANDOUBLE
        typedef FFTPlanDoubleOoura FFTPlanDouble;
    #endif
#elif (FFTSCARF_PRECISION_DEFAULT == 128)
    typedef FFTPlanOoura FFTPlanLongDoubleOoura;
    #ifndef FFTSCARF_FFTPLANLONGDOUBLE
        #define FFTSCARF_FFTPLANLONGDOUBLE
        typedef FFTPlanLongDoubleOoura FFTPlanLongDouble;
    #endif
#endif

#ifndef FFTSCARF_FFTPLAN
    #define FFTSCARF_FFTPLAN
    typedef FFTPlanOoura FFTPlan;
#endif
}

#endif // FFTSCARF_FFT_OOURA

#endif // __FFTSCARF_OOURA_H__
#ifndef __FFTSCARF_FFTREAL_H__
#define __FFTSCARF_FFTREAL_H__

#ifdef FFTSCARF_FFT_FFTREAL

#include <cmath>
#include <complex>
#include <vector>
#include <string>
#include <sstream>

#include <FFTReal/FFTReal.h>

namespace fftscarf {

template<typename _FloatType>
class FFTPlanFFTRealTemplate : public FFTPlanImplementation
{
public:
    typedef _FloatType FloatType;

private:
    ffft::FFTReal<FloatType> *m_fftreal_fft;
    std::vector<FloatType> m_signal;
    FloatType* m_fftreal_spec;
    FFTSCARF_PLAN_ACCESS_DECLARE

public:
    static inline std::string version() {
        return std::string("2.11"); // This is the current built-in version
    }
    static inline std::string libraryName() {
        std::stringstream result;
        result << "FFTReal " << version() << " (precision " << 8*sizeof(FloatType) << "b)";
        return result.str();
    }

    FFTPlanFFTRealTemplate(bool forward=true)
        : FFTPlanImplementation(forward)
    {
        m_fftreal_spec = NULL;
        m_fftreal_fft = NULL;
    }
    FFTPlanFFTRealTemplate(int n, bool forward=true)
        : FFTPlanImplementation(n, forward)
    {
        m_fftreal_spec = NULL;
        m_fftreal_fft = NULL;

        resize(n);
    }
    virtual void resize(int n)
    {
        if(n==m_size) return;

        assert(n>0);
        assert(isPow2(n));

        FFTSCARF_PLAN_ACCESS_LOCK
        m_size = n;

        if(m_fftreal_spec) delete[] m_fftreal_spec;
        m_fftreal_spec = new FloatType[m_size];

        if(m_fftreal_fft) delete m_fftreal_fft;
        m_fftreal_fft = new ffft::FFTReal<FloatType>(m_size);

        if(m_forward){
            m_signal.resize(m_size);
        }
        else{
            m_signal.resize((m_size%2==1)?(m_size-1)/2+1:m_size/2+1);
        }
        FFTSCARF_PLAN_ACCESS_UNLOCK
    }

    template<typename TypeInContainer, typename TypeOutContainer>
    void rfft(const TypeInContainer& in, TypeOutContainer& out, int dftlen=-1){
        if (!m_forward)
            throw std::string("A backward IDFT FFTPlan cannot compute the forward DFT");

        if(dftlen>0 && m_size!=dftlen)
            resize(dftlen);

        dftlen = m_size;

        int neededoutsize = (m_size%2==1)?(m_size-1)/2+1:m_size/2+1;
        if(int(out.size())!=neededoutsize)
            out.resize(neededoutsize);

        FFTSCARF_PLAN_ACCESS_LOCK
//         if(in.size()==m_size)
//             m_fftreal_fft->do_fft(m_fftreal_spec, &(in[0]));
//         else{
            int u = 0;
            if(int(m_signal.size())!=m_size)
                m_signal.resize(m_size);
            for(; u<int(in.size()); ++u)
                m_signal[u] = in[u];
            for(; u<dftlen; ++u)
                m_signal[u] = 0.0;
            m_fftreal_fft->do_fft(m_fftreal_spec, &(m_signal[0]));
//         }
        FFTSCARF_PLAN_ACCESS_UNLOCK

        out[0] = m_fftreal_spec[0]; // DC
        // TODO manage odd size
        for(int f=1; f<m_size/2; f++)
            out[f] = make_complex(m_fftreal_spec[f], -m_fftreal_spec[m_size/2+f]);
        out[m_size/2] = m_fftreal_spec[m_size/2]; // Nyquist
    }

    template<typename TypeInContainer, typename TypeOutContainer>
    void irfft(const TypeInContainer& in, TypeOutContainer& out, int winlen=-1){
        if(m_forward)
            throw std::string("A forward DFT FFTPlan cannot compute the backward IDFT");

        int dftlen = int((in.size()-1)*2);
        if(m_size!=dftlen)
            resize(dftlen);

        if(winlen==-1)
            winlen = m_size;

        if(int(out.size())!=winlen)
            out.resize(winlen);


        // TODO manage odd size
        FloatType oneoverdftlen = FloatType(1.0)/m_size;
        m_fftreal_spec[0] = in[0].real()*oneoverdftlen; // DC
        for(int f=1; f<m_size/2; f++){
            m_fftreal_spec[f] = in[f].real()*oneoverdftlen;
            m_fftreal_spec[m_size/2+f] = -in[f].imag()*oneoverdftlen;
        }
        m_fftreal_spec[m_size/2] = in[m_size/2].real()*oneoverdftlen; // Nyquist

        FFTSCARF_PLAN_ACCESS_LOCK
//         if(winlen==m_size)
//             m_fftreal_fft->do_ifft(m_fftreal_spec, &(out[0])); // IDFT
//         else{
            if(int(m_signal.size())!=m_size)
                m_signal.resize(m_size);

            m_fftreal_fft->do_ifft(m_fftreal_spec, &(m_signal[0])); // IDFT

            for(int i=0; i<winlen; i++)
                out[i] = m_signal[i];
//         }
        FFTSCARF_PLAN_ACCESS_UNLOCK
    }

    virtual ~FFTPlanFFTRealTemplate()
    {
        if(m_fftreal_fft)	delete m_fftreal_fft;
        if(m_fftreal_spec)	delete[] m_fftreal_spec;
    }
};

#ifdef FFTSCARF_PRECISION_SINGLE
    typedef FFTPlanFFTRealTemplate<float> FFTPlanSingleFFTReal;
    #ifndef FFTSCARF_FFTPLANSINGLE
        #define FFTSCARF_FFTPLANSINGLE
        typedef FFTPlanSingleFFTReal FFTPlanSingle;
    #endif
#endif
#ifdef FFTSCARF_PRECISION_DOUBLE
    typedef FFTPlanFFTRealTemplate<double> FFTPlanDoubleFFTReal;
    #ifndef FFTSCARF_FFTPLANDOUBLE
        #define FFTSCARF_FFTPLANDOUBLE
        typedef FFTPlanDoubleFFTReal FFTPlanDouble;
    #endif
#endif
#ifdef FFTSCARF_PRECISION_LONGDOUBLE
    typedef FFTPlanFFTRealTemplate<long double> FFTPlanLongDoubleFFTReal;
    #ifndef FFTSCARF_FFTPLANLONGDOUBLE
        #define FFTSCARF_FFTPLANLONGDOUBLE
        typedef FFTPlanLongDoubleFFTReal FFTPlanLongDouble;
    #endif
#endif

#if FFTSCARF_PRECISION_DEFAULT == 32
    typedef FFTPlanSingleFFTReal FFTPlanFFTReal;
#elif FFTSCARF_PRECISION_DEFAULT == 64
    typedef FFTPlanDoubleFFTReal FFTPlanFFTReal;
#elif FFTSCARF_PRECISION_DEFAULT == 128
    typedef FFTPlanLongDoubleFFTReal FFTPlanFFTReal;
#endif

#ifndef FFTSCARF_FFTPLAN
    #if FFTSCARF_PRECISION_DEFAULT == 32
        #define FFTSCARF_FFTPLAN
        typedef FFTPlanSingleFFTReal FFTPlan;
    #elif FFTSCARF_PRECISION_DEFAULT == 64
        #define FFTSCARF_FFTPLAN
        typedef FFTPlanDoubleFFTReal FFTPlan;
    #elif FFTSCARF_PRECISION_DEFAULT == 128
        #define FFTSCARF_FFTPLAN
        typedef FFTPlanLongDoubleFFTReal FFTPlan;
    #endif
#endif
}

#endif // FFTSCARF_FFT_FFTREAL

#endif // __FFTSCARF_FFTREAL_H__
#ifndef __FFTSCARF_H_FOOTER__
#define __FFTSCARF_H_FOOTER__

namespace fftscarf {

    #ifdef FFTSCARF_PRECISION_SINGLE
        #ifndef DOXYGEN_SHOULD_SKIP_THIS
        extern FFTPlanManager<FFTPlanSingle> s_pm_plan_fwd_single;
        extern FFTPlanManager<FFTPlanSingle> s_pm_plan_bck_single;
        #endif
        //! Compute the forward FFT using single precision
        template<typename TypeInContainer, typename TypeOutContainer>
        inline void rfftf(const TypeInContainer& in, TypeOutContainer& out, int dftlen){
            assert(dftlen>0);
            s_pm_plan_fwd_single.rfft(in, out, dftlen);
        }

        //! Compute the inverse FFT using single precision (Currently limited to even sizes)
        template<typename TypeInContainer, typename TypeOutContainer>
        inline void irfftf(const TypeInContainer& in, TypeOutContainer& out, int winlen=-1){
            s_pm_plan_bck_single.irfft(in, out, winlen);
        }

        //! Return a single precision FFT plan of the given size
        inline FFTPlanSingle* planf(int dftlen){
            return s_pm_plan_fwd_single.plan(dftlen);
        }
        //! Return a single precision iFFT plan of the given size
        inline FFTPlanSingle* iplanf(int dftlen){
            return s_pm_plan_bck_single.plan(dftlen);
        }

        //! Return the FFTPlanManager corresponding to single precision FFT
        inline FFTPlanManager<FFTPlanSingle>& planmanagerf(){
            return s_pm_plan_fwd_single;
        }
        //! Return the FFTPlanManager corresponding to single precision iFFT
        inline FFTPlanManager<FFTPlanSingle>& iplanmanagerf(){
            return s_pm_plan_bck_single;
        }

    #endif
    #ifdef FFTSCARF_PRECISION_DOUBLE
        #ifndef DOXYGEN_SHOULD_SKIP_THIS
        extern FFTPlanManager<FFTPlanDouble> s_pm_plan_fwd_double;
        extern FFTPlanManager<FFTPlanDouble> s_pm_plan_bck_double;
        #endif
        //! Compute the forward FFT using double precision
        template<typename TypeInContainer, typename TypeOutContainer>
        inline void rfftd(const TypeInContainer& in, TypeOutContainer& out, int dftlen){
            assert(dftlen>0);
            s_pm_plan_fwd_double.rfft(in, out, dftlen);
        }

        //! Compute the inverse FFT using double precision (Currently limited to even sizes)
        template<typename TypeInContainer, typename TypeOutContainer>
        inline void irfftd(const TypeInContainer& in, TypeOutContainer& out, int winlen=-1){
            s_pm_plan_bck_double.irfft(in, out, winlen);
        }

        //! Return a double precision FFT plan of the given size
        inline FFTPlanDouble* pland(int dftlen){
            return s_pm_plan_fwd_double.plan(dftlen);
        }
        //! Return a double precision iFFT plan of the given size
        inline FFTPlanDouble* ipland(int dftlen){
            return s_pm_plan_bck_double.plan(dftlen);
        }

        //! Return the FFTPlanManager corresponding to double precision FFT
        inline FFTPlanManager<FFTPlanDouble>& planmanagerd(){
            return s_pm_plan_fwd_double;
        }
        //! Return the FFTPlanManager corresponding to double precision iFFT
        inline FFTPlanManager<FFTPlanDouble>& iplanmanagerd(){
            return s_pm_plan_bck_double;
        }

    #endif
    #ifdef FFTSCARF_PRECISION_LONGDOUBLE
        #ifndef DOXYGEN_SHOULD_SKIP_THIS
        extern FFTPlanManager<FFTPlanLongDouble> s_pm_plan_fwd_long_double;
        extern FFTPlanManager<FFTPlanLongDouble> s_pm_plan_bck_long_double;
        #endif
        //! Compute the forward FFT using long double precision
        template<typename TypeInContainer, typename TypeOutContainer>
        inline void rfftl(const TypeInContainer& in, TypeOutContainer& out, int dftlen){
            assert(dftlen>0);
            s_pm_plan_fwd_long_double.rfft(in, out, dftlen);
        }

        //! Compute the inverse FFT using long double precision (Currently limited to even sizes)
        template<typename TypeInContainer, typename TypeOutContainer>
        inline void irfftl(const TypeInContainer& in, TypeOutContainer& out, int winlen=-1){
            s_pm_plan_bck_long_double.irfft(in, out, winlen);
        }

        //! Return a long double precision FFT plan of the given size
        inline FFTPlanLongDouble* planl(int dftlen){
            return s_pm_plan_fwd_long_double.plan(dftlen);
        }
        //! Return a long double precision iFFT plan of the given size
        inline FFTPlanLongDouble* iplanl(int dftlen){
            return s_pm_plan_bck_long_double.plan(dftlen);
        }

        //! Return the FFTPlanManager corresponding to double precision FFT
        inline FFTPlanManager<FFTPlanLongDouble>& planmanagerl(){
            return s_pm_plan_fwd_long_double;
        }
        //! Return the FFTPlanManager corresponding to double precision iFFT
        inline FFTPlanManager<FFTPlanLongDouble>& iplanmanagerl(){
            return s_pm_plan_bck_long_double;
        }
    #endif

    //! Compute the forward FFT using default precision
    template<typename TypeInContainer, typename TypeOutContainer>
    inline void rfft(const TypeInContainer& in, TypeOutContainer& out, int dftlen){
        #if FFTSCARF_PRECISION_DEFAULT == 32
            fftscarf::rfftf(in, out, dftlen);
        #elif FFTSCARF_PRECISION_DEFAULT == 64
            fftscarf::rfftd(in, out, dftlen);
        #elif FFTSCARF_PRECISION_DEFAULT == 128
            fftscarf::rfftl(in, out, dftlen);
        #endif
    }

    //! Compute the inverse FFT using default precision (Currently limited to even sizes)
    template<typename TypeInContainer, typename TypeOutContainer>
    inline void irfft(const TypeInContainer& in, TypeOutContainer& out, int winlen=-1){
        #if FFTSCARF_PRECISION_DEFAULT == 32
            fftscarf::irfftf(in, out, winlen);
        #elif FFTSCARF_PRECISION_DEFAULT == 64
            fftscarf::irfftd(in, out, winlen);
        #elif FFTSCARF_PRECISION_DEFAULT == 128
            fftscarf::irfftl(in, out, winlen);
        #endif
    }

    //! Return a default precision FFT plan of the given size
    inline FFTPlan* plan(int dftlen){
        #if FFTSCARF_PRECISION_DEFAULT == 32
            return fftscarf::planf(dftlen);
        #elif FFTSCARF_PRECISION_DEFAULT == 64
            return fftscarf::pland(dftlen);
        #elif FFTSCARF_PRECISION_DEFAULT == 128
            return fftscarf::planl(dftlen);
        #endif
    }
    //! Return a default precision iFFT plan of the given size
    inline FFTPlan* iplan(int dftlen){
        #if FFTSCARF_PRECISION_DEFAULT == 32
            return fftscarf::iplanf(dftlen);
        #elif FFTSCARF_PRECISION_DEFAULT == 64
            return fftscarf::ipland(dftlen);
        #elif FFTSCARF_PRECISION_DEFAULT == 128
            return fftscarf::iplanl(dftlen);
        #endif
    }

    //! Return the FFTPlanManager corresponding to double precision FFT
    #if FFTSCARF_PRECISION_DEFAULT == 32
        inline FFTPlanManager<FFTPlan>& planmanager(){return fftscarf::planmanagerf();}
    #elif FFTSCARF_PRECISION_DEFAULT == 64
        inline FFTPlanManager<FFTPlan>& planmanager(){return fftscarf::planmanagerd();}
    #elif FFTSCARF_PRECISION_DEFAULT == 128
        inline FFTPlanManager<FFTPlan>& planmanager(){return fftscarf::planmanagerl();}
    #endif

    //! Return the FFTPlanManager corresponding to double precision iFFT
    #if FFTSCARF_PRECISION_DEFAULT == 32
        inline FFTPlanManager<FFTPlan>& iplanmanager(){return fftscarf::iplanmanagerf();}
    #elif FFTSCARF_PRECISION_DEFAULT == 64
        inline FFTPlanManager<FFTPlan>& iplanmanager(){return fftscarf::iplanmanagerd();}
    #elif FFTSCARF_PRECISION_DEFAULT == 128
        inline FFTPlanManager<FFTPlan>& iplanmanager(){return fftscarf::iplanmanagerl();}
    #endif


    //! Write some compile-time information to the given stream.
    static inline void write_compile_info(std::ostream& out){
        out << "    FFTScarf version: " << fftscarf::version()  << std::endl;
        #ifdef FFTSCARF_LICENSE_GPLENFORCED
        out << "    License: ATTENTION: GPL is enforced on this software" << std::endl;
        #endif
        #ifdef FFTSCARF_PLAN_PROTECTACCESS
        out << "    Plan access is protected" << std::endl;
        #else
        out << "    Plan access is NOT protected" << std::endl;
        #endif

        out << "    Available FFT implementations: " << std::endl;

        #ifdef FFTSCARF_FFT_IPP
            #ifdef FFTSCARF_PRECISION_SINGLE
                out << "        " << fftscarf::FFTPlanSingleIPP::libraryName() << std::endl;
            #endif
            #ifdef FFTSCARF_PRECISION_DOUBLE
                out << "        " << fftscarf::FFTPlanDoubleIPP::libraryName() << std::endl;
            #endif
        #endif
        #ifdef FFTSCARF_FFT_FFTS
            out << "        " << fftscarf::FFTPlanFFTS::libraryName() << std::endl;
        #endif
        #ifdef FFTSCARF_FFT_PFFFT
            out << "        " << fftscarf::FFTPlanPFFFT::libraryName() << std::endl;
        #endif
        #ifdef FFTSCARF_FFT_FFTW3
            #ifdef FFTSCARF_PRECISION_SINGLE
                out << "        " << fftscarf::FFTPlanSingleFFTW3::libraryName() << std::endl;
            #endif
            #ifdef FFTSCARF_PRECISION_DOUBLE
                out << "        " << fftscarf::FFTPlanDoubleFFTW3::libraryName() << std::endl;
            #endif
            #ifdef FFTSCARF_PRECISION_LONGDOUBLE
                out << "        " << fftscarf::FFTPlanLongDoubleFFTW3::libraryName() << std::endl;
            #endif
        #endif
        #ifdef FFTSCARF_FFT_OOURA
            out << "        " << fftscarf::FFTPlanOoura::libraryName() << std::endl;
        #endif
        #ifdef FFTSCARF_FFT_FFTREAL
            #ifdef FFTSCARF_PRECISION_SINGLE
                out << "        " << fftscarf::FFTPlanSingleFFTReal::libraryName() << std::endl;
            #endif
            #ifdef FFTSCARF_PRECISION_DOUBLE
                out << "        " << fftscarf::FFTPlanDoubleFFTReal::libraryName() << std::endl;
            #endif
            #ifdef FFTSCARF_PRECISION_LONGDOUBLE
                out << "        " << fftscarf::FFTPlanLongDoubleFFTReal::libraryName() << std::endl;
            #endif
        #endif
    //    #ifdef FFTSCARF_FFT_DJBFFT
    //        push_back(fftscarf::FFTPlanDJBFFT::libraryName());
    //    #endif
        #ifdef FFTSCARF_FFT_DFT
            #ifdef FFTSCARF_PRECISION_SINGLE
                out << "        " << fftscarf::FFTPlanSingleDFT::libraryName() << std::endl;
            #endif
            #ifdef FFTSCARF_PRECISION_DOUBLE
                out << "        " << fftscarf::FFTPlanDoubleDFT::libraryName() << std::endl;
            #endif
            #ifdef FFTSCARF_PRECISION_LONGDOUBLE
                out << "        " << fftscarf::FFTPlanLongDoubleDFT::libraryName() << std::endl;
            #endif
        #endif


        #ifdef FFTSCARF_FFTPLANSINGLE
        out << "    Default FFTPlanSingle: " << FFTPlanSingle::libraryName() << std::endl;
        #endif
        #ifdef FFTSCARF_FFTPLANDOUBLE
        out << "    Default FFTPlanDouble: " << FFTPlanDouble::libraryName() << std::endl;
        #endif
        #ifdef FFTSCARF_FFTPLANLONGDOUBLE
        out << "    Default FFTPlanLongDouble: " << FFTPlanLongDouble::libraryName() << std::endl;
        #endif
        #ifdef FFTSCARF_FFTPLAN
        out << "    Default FFTPlan: " << FFTPlan::libraryName() << std::endl;
        #endif
        #if FFTSCARF_PRECISION_DEFAULT == 32
        out << "    Default precision: single (float 32b)" << std::endl;
        #elif FFTSCARF_PRECISION_DEFAULT == 64
        out << "    Default precision: double (float 64b)" << std::endl;
        #elif FFTSCARF_PRECISION_DEFAULT == 128
        out << "    Default precision: long double (float 128b)" << std::endl;
        #endif
    }

    //! Write some compile-time information to the given stream.
    static inline void write_compile_info_json(std::ostream& out){
        out << "      \"version\": \"" << fftscarf::version() << "\"," << std::endl;
        #ifdef FFTSCARF_LICENSE_GPLENFORCED
        out << "      \"license\": \"ATTENTION: GPL is enforced on this software\"," << std::endl;
        #else
        out << "      \"license\": \"GPL not enforced\"," << std::endl;
        #endif
        #ifdef FFTSCARF_PLAN_PROTECTACCESS
        out << "      \"plan_access\": \"protected\"," << std::endl;
        #else
        out << "      \"plan_access\": \"unprotected\"," << std::endl;
        #endif

        out << "      \"implementations\": {" << std::endl;

        #ifdef FFTSCARF_FFT_IPP
            #ifdef FFTSCARF_PRECISION_SINGLE
                out << "        \"IPP_SINGLE\": \"" << fftscarf::FFTPlanSingleIPP::libraryName() << "\"," << std::endl;
            #endif
            #ifdef FFTSCARF_PRECISION_DOUBLE
                out << "        \"IPP_DOUBLE\": \"" << fftscarf::FFTPlanDoubleIPP::libraryName() << "\"," << std::endl;
            #endif
        #endif
        #ifdef FFTSCARF_FFT_FFTS
            out << "        \"FFTS\": \"" << fftscarf::FFTPlanFFTS::libraryName() << "\"," << std::endl;
        #endif
        #ifdef FFTSCARF_FFT_PFFFT
            out << "        \"PFFFT\": \"" << fftscarf::FFTPlanPFFFT::libraryName() << "\"," << std::endl;
        #endif
        #ifdef FFTSCARF_FFT_FFTW3
            #ifdef FFTSCARF_PRECISION_SINGLE
                out << "        \"FFW3_SINGLE\": \"" << fftscarf::FFTPlanSingleFFTW3::libraryName() << "\"," << std::endl;
            #endif
            #ifdef FFTSCARF_PRECISION_DOUBLE
                out << "        \"FFW3_DOUBLE\": \"" << fftscarf::FFTPlanDoubleFFTW3::libraryName() << "\"," << std::endl;
            #endif
            #ifdef FFTSCARF_PRECISION_LONGDOUBLE
                out << "        \"FFW3_LONGDOUBLE\": \"" << fftscarf::FFTPlanLongDoubleFFTW3::libraryName() << "\"," << std::endl;
            #endif
        #endif
        #ifdef FFTSCARF_FFT_OOURA
            out << "        \"OOURA\": \"" << fftscarf::FFTPlanOoura::libraryName() << "\"," << std::endl;
        #endif
        #ifdef FFTSCARF_FFT_FFTREAL
            #ifdef FFTSCARF_PRECISION_SINGLE
                out << "        \"FFTREAL_SINGLE\": \"" << fftscarf::FFTPlanSingleFFTReal::libraryName() << "\"," << std::endl;
            #endif
            #ifdef FFTSCARF_PRECISION_DOUBLE
                out << "        \"FFTREAL_DOUBLE\": \"" << fftscarf::FFTPlanDoubleFFTReal::libraryName() << "\"," << std::endl;
            #endif
            #ifdef FFTSCARF_PRECISION_LONGDOUBLE
                out << "        \"FFTREAL_LONGDOUBLE\": \"" << fftscarf::FFTPlanLongDoubleFFTReal::libraryName() << "\"," << std::endl;
            #endif
        #endif
    //    #ifdef FFTSCARF_FFT_DJBFFT
    //        push_back(fftscarf::FFTPlanDJBFFT::libraryName());
    //    #endif
        #ifdef FFTSCARF_FFT_DFT
            #ifdef FFTSCARF_PRECISION_SINGLE
                out << "        \"DFT_SINGLE\": \"" << fftscarf::FFTPlanSingleDFT::libraryName() << "\"," << std::endl;
            #endif
            #ifdef FFTSCARF_PRECISION_DOUBLE
                out << "        \"DFT_DOUBLE\": \"" << fftscarf::FFTPlanDoubleDFT::libraryName() << "\"," << std::endl;
            #endif
            #ifdef FFTSCARF_PRECISION_LONGDOUBLE
                out << "        \"DFT_LONGDOUBLE\": \"" << fftscarf::FFTPlanLongDoubleDFT::libraryName() << "\"," << std::endl;
            #endif
        #endif

            out << "        \"eol\": \"eol\"" << std::endl;
        out << "      }," << std::endl;

        out << "      \"precisions\": [" << std::endl;
            #ifdef FFTSCARF_FFTPLANSINGLE
            out << "        \"single\"," << std::endl;
            #endif
            #ifdef FFTSCARF_FFTPLANDOUBLE
            out << "        \"double\"," << std::endl;
            #endif
            #ifdef FFTSCARF_FFTPLANLONGDOUBLE
            out << "        \"longdouble\"," << std::endl;
            #endif
            out << "        \"eol\"" << std::endl;
        out << "      ]," << std::endl;

        #ifdef FFTSCARF_FFTPLANSINGLE
        out << "      \"default_FFTPlanSingle\": \"" << FFTPlanSingle::libraryName() << "\"," << std::endl;
        #endif
        #ifdef FFTSCARF_FFTPLANDOUBLE
        out << "      \"default_FFTPlanDouble\": \"" << FFTPlanDouble::libraryName() << "\"," << std::endl;
        #endif
        #ifdef FFTSCARF_FFTPLANLONGDOUBLE
        out << "      \"default_FFTPlanLongDouble\": \"" << FFTPlanLongDouble::libraryName() << "\"," << std::endl;
        #endif
        #ifdef FFTSCARF_FFTPLAN
        out << "      \"default_FFTPlan\": \"" << FFTPlan::libraryName() << "\"," << std::endl;
        #endif
        #if FFTSCARF_PRECISION_DEFAULT == 32
        out << "      \"default_precision\": \"single\"" << std::endl;
        #elif FFTSCARF_PRECISION_DEFAULT == 64
        out << "      \"default_precision\": \"double\"" << std::endl;
        #elif FFTSCARF_PRECISION_DEFAULT == 128
        out << "      \"default_precision\": \"longdouble\"" << std::endl;
        #endif
    }
}

#endif // __FFTSCARF_H_FOOTER__
